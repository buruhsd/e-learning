<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['name', 'images', 'url', 'status'];

    public function courses(){
      return $this->hasMany('App\Course');
    }


    public function getImageFullPath()
	{
		$value = $this->images;
		if ($value == null)
			return null;

		$path = Config::get('storage.subject');		
		return "{$path}/{$value}";
	}
}



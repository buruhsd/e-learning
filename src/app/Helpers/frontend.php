<?php
\Html::macro('smartNav', function($url, $title) {
$class = $url == request()->url() ? 'active' : '';
return "<li><a class=\"$class\" href=\"$url\">$title</a></li>";
});
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Laratrust;
use Mail;

class UserController extends Controller
{
    //

	public function index(Request $request){
        $search = $request->search;
        $state = $request->suspended;

        if (Laratrust::hasRole('admin')){
        $users = User::where('role', '=', 'teacher')
            ->orderBy('id');
        if($search){
            $users = $users->where(function($q) use ($search) {
                $q->where('role', '=', 'teacher')
                ->where('name', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%");
            });
        }
        if($state && $state != 'ALL'){
            $users = $users->where('suspended','=',$state);
        }

        $users = $users->paginate(10)->appends($request->all());

		return view('user.list',compact('users'));
        }
        return view('errors.403');
	}

	public function add(){
        if (Laratrust::hasRole('admin')){
		return view('user.add');
        }
        return view('errors.403');
	}

	public function store(Request $request){
        $password = str_random(6);

		$this->validate($request, [
            'name'=>'required',
            'email'=>'required|unique:users,email|email',
            //'password'=>'required',
            ]);
        //var_dump($request->suspended); die();

        //var_dump($request->suspended); die();

		$user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($password),
            'role' => 'teacher',
            'suspended' => $request->suspended,
        ]);

        $role = Role::where('name','teacher')->first();
        $user->attachRole($role);

        //var_dump($user); die();

        Mail::send('mail.invite', compact('user', 'password'), function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Semesta Web TV [You Have been Added on Semesta]');
        });

        return redirect()->action('UserController@index')
                        ->with('UPDATE.OK', true);

	}

    public function edit(Request $request, $id){
        $user = User::find($id);
        if(Laratrust::hasRole('admin')){
        return view('user.edit')->with('user', $user);
        }
        return view('errors.403');
    }

    public function update(Request $request, $id){
      $user = User::find($id);

      $this->validate($request, [
            'name'=>'required',
            'email'=>'required|email|unique:users,email,' . $id,
            
            ]);

      $user->fill([
        'name' => $request->name,
        'email' => $request->email,
        'role' => 'teacher',
        'suspended' => $request->suspended,
        
        //'password' => bcrypt('password'),
        ]);
      $user->save();

      return redirect()->action('UserController@index')
                      ->with('UPDATE.OK', true);
    }

    public function delete($id){
        $user = User::find($id);
        $user->delete();

        return redirect()->action('UserController@index')
                        ->with('DELETE.OK', true);
    }

    public function memberlist(Request $request){
        $search = $request->search;
        $state = $request->suspended;

        if (Laratrust::hasRole('admin')){
        $users = User::where('role', '=', 'member')
            ->orderBy('name');
        if($search){
            $users = $users->where(function($q) use ($search) {
                $q->where('name', 'like', "%$search%")
                ->orWhere('email', 'like', "%$search%");
            });
        }
        if($state && $state != 'ALL'){
            $users = $users->where('suspended','=',$state);
        }

        $users = $users->paginate(10)->appends($request->all());

        return view('member.list')
            ->with('users', $users)
            ->with('state', $state);
        }
        return view('errors.403');
    }

    public function suspend($id){
        $user = User::find($id);
        $user = User::where('id', $id)->update(['suspended' => false]);

        return redirect()->action('UserController@memberlist')
                        ->with('SUSPEND.OK', true);
    }

    public function unsuspend($id){
        $user = User::find($id);
        $user = User::where('id', $id)->update(['suspended' => true]);

        return redirect()->action('UserController@memberlist')
                        ->with('UNSUSPEND.OK', true);
    }
}

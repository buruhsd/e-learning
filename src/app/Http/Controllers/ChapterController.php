<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chapter;
use App\Course;
use App\Section;
use App\SectionContent;
use App\User;
use Laratrust;
use Role;
use Auth;

class ChapterController extends Controller
{
    //
    public function index(){
    	$search = \Request::get('search');

        $chapter = Chapter::where('name','like','%'.$search.'%')
        //$users = Role::where('name', 'teacher')
            ->orderBy('id', 'DESC')->paginate(10);

		return view('chapter.list',compact('chapter'));
    }

    public function add(){
    	return view('chapter.add');
    }

    public function store(Request $request){

    	$this->validate($request, [
            'name'=>'required',
            'course_id' =>'required',
            ]);

    	$chapter = Chapter::create([
            'name' => $request->name,
            'course_id' => $request->course_id,
            'description' => $request->description,
        ]);

        return redirect()->back()
                        ->with('UPDATE.OK', true);
    }

    public function edit(Request $request, $id){
    	$chapter = Chapter::find($id);

    	return view('chapter.edit', compact('chapter'));
    }

    public function update(Request $request, $id){
    	$chapter = Chapter::find($id);       

        //var_dump($request->name); die();

    	$chapter ->fill([
            'name' => $request->name,
            'course_id' => $request->course_id,
            'description' => $request->description,
        ]);
        $chapter->save();

        //var_dump($chapter); die();

    	return redirect()->action('ChapterController@chaptermanager', [$request->course_id])
    		->with('UPDATE.OK', true);
    }

    public function delete($id){
    	$chapter = Chapter::find($id);
    	$chapter->delete();

    	return redirect()->back()
    		->with('UPDATE.OK', true);
    }


    public function chaptermanager($id){
        $course = Course::find($id);
        $chapter_id = Chapter::find($id);
        $chapters = Chapter::where('course_id', $id)->get();

        $id_chapter_arr = [];
        foreach ($chapters as $key => $chapter) {
            $id_chapter_arr[] = $chapter->id;
        }

        //var_dump($sections); die();
        
        if(Laratrust::hasRole('admin')){
            return view('chapter.list')
            ->with('course', $course)
            ->with('chapters', $chapters);
        }
        $teacher = Auth::user()->id;
        if($course->teacher_id == $teacher ){
        return view('chapter.list')
            ->with('course', $course)
            ->with('chapters', $chapters);
      }
      return view('errors.403');
    }

    public function ajax($id_chapter){
        $data = Chapter::find($id_chapter);

      return response()->json($data);
    }

    public function content($id_content){
        $data = SectionContent::find($id_content);

      return response()->json($data);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use App\Rate;
use Auth;
use App\Counter;
use App\Course;

class SectionController extends Controller
{
    //
    public function index(){
    	$search = \Request::get('search');

        $section = Section::where('name','like','%'.$search.'%')
        //$users = Role::where('name', 'teacher')
            ->orderBy('id', 'DESC')->paginate(10);

		return view('section.list',compact('section'));
    }

    public function add(){
    	return view('section.add');
    }

    public function store(Request $request){
        //var_dump($request);
    	$section = Section::create([
            'name' => $request->name,
            'chapter_id' => $request->chapter_id,
            'description' => $request->description,
        ]);

        //var_dump($section); die();
        return redirect()->back()
                        ->with('UPDATE.OK', true);
    }

    public function edit(Request $request, $id){
    	$section = Section::find($id);

    	return view('section.edit', compact('section'));
    }

    public function update(Request $request, $id){
    	$section = Section::find($id);

    	$this->validate($request, [
            'name'=>'required',
            'chapter_id' =>'required',
            ]);

    	$section ->fill([
            'name' => $request->name,
            'chapter_id' => $request->chapter_id,
            'description' => $request->description,
        ]);
        $section->save();

    	return redirect()->back()
    		->with('UPDATE.OK', true);
    }

    public function delete($id){
    	$section = Section::find($id);
    	$section->delete();

    	return redirect()->back()
    		->with('UPDATE.OK', true);
    }

    public function ajax($id_section){
        $data = Section::find($id_section);

      return response()->json($data);
    }

    private function hit_counter(){

    }

    public function show($id){
        $counter =  new Counter;
        $section = Section::with('section_contents.content','chapter.course.subject')->find($id);
        $section->view_count = Counter::where('section_id',$id)->count();
        $course = Course::findOrFail($section->chapter->course->id);
        $member = $course->students()->where('users.id', Auth::user()->id)->count();
        if($member>0 || Auth::user()->role == 'teacher' ||  Auth::user()->role == 'admin'){
        }else{
          return redirect()->action(
              'CourseController@show', ['id' => $section->id]
          );
        }
        $counter->section_id = $section->id;
        $counter->hit();
        $rate = Rate::where('section_id',$id);
        $section->rates = ($rate->avg('value')?$rate->avg('value'):5);
        $userrate = $rate->where('user_id',Auth::user()->id);
        $section->user_rate = ($userrate->avg('value')?$userrate->avg('value'):0);
        //return response()->json($section);

        if (($section->chapter->course->ladder->status == 'inactive') or ($section->chapter->course->subject->status == 'inactive')) {
            # code...
            return view('errors.403');
        }
        return view('frontend.section_class', compact('section'));
    }
}

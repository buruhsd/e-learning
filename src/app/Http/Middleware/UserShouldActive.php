<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laratrust;

class UserShouldActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (Auth::check() && !Auth::user()->suspended) {
            if(Laratrust::hasRole('member')){
            $link = url('auth/send-verification').'?email='.urlencode(Auth::user()->email);
            Auth::logout();

            Session::flash("flash_notification", [
                "level"   => "warning",
                "message" => "We've been send verification code to your email.
          <a class='alert-link' href='$link'>send again</a>."
            ]);
        }
        Auth::logout();
        Session::flash("flash_notification", [
                "level"   => "warning",
                "message" => "Your Account is suspended."
            ]);

            return redirect('/login');
        }

        return $response;
    }
}

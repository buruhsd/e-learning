<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	
	protected $fillable = [
	'featured_subject_1',
	'featured_subject_2',
	'featured_subject_3',
	'featured_subject_4',
	'address_1',
	'address_2',
	'address_3',
	'phone',
	'fax',
	'email',
	'facebook',
	'twitter',
	'youtube',
	'instagram',
	'video_url',
	];
}

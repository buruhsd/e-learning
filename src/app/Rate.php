<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = ['section_id, user_id, value'];

    public function section(){
    	return $this->belongsTo('App\Section');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }
}

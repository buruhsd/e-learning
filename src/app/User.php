<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'gender', 'phone', 'ladder_id', 'city_id','suspended'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'suspended' => 'boolean',
    ];

    public function city(){
      return $this->hasOne('App\City');
    }

    public function courses(){
      return $this->hasMany('App\Course');
    }

    public function feedbacks(){
      return $this->hasMany('App\Feedback');
    }

    public function generateVerificationToken()
    {
        $token = $this->verification_token;
        if (!$token) {
            $token = str_random(40);
            $this->verification_token = $token;
            $this->save();
        }
        return $token;
    }

    public function sendVerification()
    {
        $token = $this->generateVerificationToken();
        $user = $this;
        Mail::send('auth.emails.verification', compact('user', 'token'), function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('[Semesta Web TV] Verification Account');
        });
    }

    public function verify()
    {
        $this->suspended = 1;
        $this->verification_token = null;
        $this->save();
    }

    /*
    public function getLastStatusAttribute(){
      $statuses = $this->statuses()->orderBy('created_at', 'desc')->first();

      if(count($statuses) == 0)
        return "";

      return $statuses->status_name;
    }
    */

    public function count(){
      $members = $this->users()->where('role','=', 'member')->count();
        
    }


}

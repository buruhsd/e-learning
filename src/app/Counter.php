<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    //
    public function hit(){
      $this->ip_address = $_SERVER['REMOTE_ADDR'];
      $this->save();
    }

    public function section(){
      return $this->belongsTo('App\Section');
    }
}

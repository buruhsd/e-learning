<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ladder extends Model
{
    protected $fillable = ['name', 'status'];

    public function users(){
      return $this->hasMany('App\User');
    }
    public function courses(){
      return $this->hasMany('App\Course');
    }
}

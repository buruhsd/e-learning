<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['comment', 'status', 'section_id', 'user_id', 'parent_id'];

    public function section(){
    	return $this->belongsTo('App\Section');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function replies()
    {
        return $this->hasMany('App\Feedback', 'parent_id');
    }
}

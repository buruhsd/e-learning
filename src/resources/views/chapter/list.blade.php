@extends('master', ['active' => 'chapter'])
@section('sidebar')
@include('chapter.sidebar', ['active' => 'chapter'])
@endsection
@section('content')

<div class="container-fluid">
    <h3>{{ $course->name }}</h3>
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Subject</p>
                    <p class="medium-title">{{ $course->subject->name }}</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Ladder</p>
                    <p class="medium-title">{{ $course->ladder->name }}</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Teacher</p>
                    <p class="medium-title">{{ $course->teacher->name }}</p>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <p>Description</p>
            <blockquote class="blockquote-custom">
                {{ $course->description }}
            </blockquote>
        </div>
    </div>

    <h3>Chapter</h3>
    <hr>
    <div class="row">
        <div class="col-md-3">
            <button type="button" class="btn btn-success btn-lg col-sm-12 text-center add-modal" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $course->id }}" data-target="#addChapter"><i class="glyphicon glyphicon-plus"></i> Add Chapter</button>
            <div class="clearfix"></div>
            <br>
            <div class="list-group">
                @if(count($chapters) == 0)
                <tr>
                    <td colspan="5">There is no data.</td>
                </tr>
                @include('chapter.content-modal')
                @endif
                <!-- <?php $index=1; ?> -->
                <ul class="nav nav-pills nav-stacked" role="tablist">
                    @foreach ($chapters as $key => $chapter)
                    @if ($key == 0)
                    <li role="presentation" class="active">
                        <a href="#{{$chapter->id}}" aria-controls="{{$chapter->id}}" role="tab" data-toggle="tab" data-id="{{$chapter->id}}">{{ $chapter->name }}</a>
                    </li>
                    @else
                    <li role="presentation">
                        <a href="#{{$chapter->id}}" data-id= "{{$chapter->id}}" aria-controls="{{$chapter->id}}" role="tab" data-toggle="tab">{{ $chapter->name }}</a>
                    </li>
                    @endif
                    <?php $key++; ?>
                    @endforeach
                </ul>

            </div>
        </div>
        <div class="col-md-9 tab-content" style="border-left: 1px solid #DDD;">
            @foreach($chapters as $key2 => $chapter)
            <div class="panel-default-chapter fade {{ $key2==0 ? "active in" : "" }}" role="tabpanel{{ $chapter->id }}" id="{{ $chapter->id }}">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <input type="hidden" class="id_chapter" name="id_chapter" value="0">
                        <div class="row">
                            <div class="col-xs-9" >
                                <p>Chapter Name</p>
                                <p id="chapterTitle" class="medium-title">{{$chapter->name}}</p>
                            </div>
                            <div class="col-xs-3 text-right">
                                <a class="btn btn-sm btn-default show-modal" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $chapter->id }}" data-name="{{ $chapter->name }}" data-description="{{$chapter->description}}" data-course="{{ $chapter->course_id }}" data-target="#showModal" data-href="{{ action('ChapterController@update', [$chapter->id]) }}">Edit</a>
                                <a class="btn btn-sm btn-danger" href="{{ action('ChapterController@delete', $chapter->id) }}" onclick="return confirm('Are you sure you want to delete this item?')">Delete</a>
                            </div>
                        </div>
                        <p>Description</p>
                        <blockquote class="blockquote-custom">
                            {{$chapter->description}}
                        </blockquote>
                    </div>
                </div>
                {{-- section --}}
                <div class="row">
                    <div class="col-xs-6">
                        <p style="font-size: 20px;"> Section(s)</p>
                    </div>
                    <div class="col-xs-6 text-right">
                        <button type="button" class="btn btn-success btn-sm section-modal" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $chapter->id }}" data-target="#addSection"><i class="glyphicon glyphicon-plus"></i> Add Section</button>
                    </div>
                </div>
                <div>
                    <div class="panel-group" id="accordion">
                        @foreach($chapter->sections as $key3 => $section)
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$section->id}}">
                                    {{ $section->name }}</a>
                                </h4>
                                
                            </div>
                            <div id="collapse{{$section->id}}" class="panel-collapse collapse {{ $key3==0 ? "in" : "" }}">
                              <div class="panel-body">
                                <blockquote class="blockquote-custom">
                                    {{$section->description}}
                                </blockquote>
                                <button type="button" class="btn btn-success btn-sm content-modal" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $section->id }}" data-target="#addContent"><i class="glyphicon glyphicon-plus"></i>  Add Content</button>
                                <hr>
                                <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                        <th style="width: 50px;">NO</th>
                                        <th>Content Name</th>
                                        <th>Description</th>
                                        <th>File</th>
                                        <th class="text-center" width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($section->contents) == 0)
                                    <tr>
                                        <td colspan="5">There is no data.</td>
                                    </tr>
                                    @endif
                                    @foreach($section->section_contents as $key4 => $content)
                                    <tr>
                                        <td>{{++$key4}}</td>
                                        <td>{{$content->name}}</td>
                                        <td>{{$content->description}}</td>
                                        <td>{{$content->content->name}}</td>
                                        <td class="text-center">
                                        <a class="sectionContentModal" href="#" data-toggle="modal" data-name="{{$content->name}}" data-description="{{$content->description}}" data-content="{{$content->content->name}}" data-contentid="{{$content->content->id}}" data-section="{{$content->id}}" data-href="{{ action('ContentController@updateSectionContent', [$content->id]) }}" data-target="#sectionContentModal" href="#">edit</a> |     
                                        <a href="{{action('ContentController@deleteSectionContent', $content->id)}}" onclick="return confirm('Are you sure you want to delete this item?')">remove</a>
                                        </td>
                                    </tr>
                                    @include('chapter.content-modal')

                                    @endforeach
                                </tbody>
                            </table>
                            <div>
                                <a class="editsection-modal" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-name="{{$section->name}}" data-description="{{$section->description}}" data-chapter="{{$section->chapter_id}}"  data-target="#sectionModal" data-href="{{ action('SectionController@update', [$section->id]) }}">edit</a> | <a href="{{ action('SectionController@delete', $section->id) }}" onclick="return confirm('Are you sure you want to delete this item?')">delete</a> 
                            </div>
                        </div>
                    </div>
                </div>
                @include('chapter.content-modal')
                <!-- @include('chapter.modal') -->
                @endforeach
            </div>
        </div>

    </div>
    @include('chapter.content-modal')
    @endforeach
</div>
</div>

</div>
@endsection


@section('content-js')
<script>
    $(function () {
        CreatePicklist('content', '/admin/content/list?');
        CreatePicklist('editcontent', '/admin/content/list?');
    });
    </script>
<script>
    $('.show-modal').on('click', function(){

        var id = $(this).data('id');
        $('#data_id').val(id);
        var name = $(this).data('name');
        $('#chapter_name').val(name);
        var description = $(this).data('description');
        $('#chapter_description').val(description);
        var course_id = $(this).data('course');
        $('#chapter_course').val(course_id);
        var href = $(this).data('href');
        $('#editModalForm').attr('action', href);
    });

    $('.editsection-modal').on('click', function(){

        var id = $(this).data('id');
        $('#data_id').val(id);
        var name = $(this).data('name');
        $('#section_name').val(name);
        var description = $(this).data('description');
        $('#section_description').val(description);
        var chapter_id = $(this).data('chapter');
        $('#section_chapter').val(chapter_id);
        var href = $(this).data('href');
        $('#editSectionForm').attr('action', href);
        //$('#editModalForm').attr('action', href);
        console.log(href);
    });

    $('.sectionContentModal').on('click', function(){

        var id = $(this).data('id');
        $('#data_id').val(id);
        var name = $(this).data('name');
        $('#section_content_name').val(name);
        var description = $(this).data('description');
        $('#section_content_description').val(description);
        var content = $(this).data('content');
        $('#editcontentName').val(content);
        var content_id = $(this).data('contentid');
        $('#editcontentId').val(content_id);
        var section = $(this).data('section');
        $('#section').val(section);
        var href = $(this).data('href');
        $('#action').attr('action', href);
        //$('#editModalForm').attr('action', href);
        console.log(href);
    });

    $('.add-modal').on('click', function(){

        var id = $(this).data('id');
        $('#course_id').val(id);
    })

    $('.section-modal').on('click', function(){

        var id = $(this).data('id');
        $('#chapter_id').val(id);
    })

    $('.content-modal').on('click', function(){

        var id = $(this).data('id');
        $('.section_id').val(id);
    })
</script>

@endsection

<!-- @section('content-modal')
-->
<!-- <!-- <div class="modal fade" id="addChapter" tabindex="-1" role="dialog" aria-labelledby="addChapterLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addChapterLabel">Add Chapter</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('ChapterController@store'),
                    'method' => 'post'
                    ])
                    !!}

                     @include('form.text', [
                        'field' => 'name',
                        'label' => 'Name',
                        'placeholder' => 'Name',
                        'default' => ''
                    ])


                    @include('form.textarea', [
                        'field' => 'description',
                        'label' => 'Description',
                        'placeholder' => 'Description',
                        'default' => ''
                    ])

                <input type="hidden" name="course_id" id="course_id">

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>
-->

<!--<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Chapter</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => '',
                    'method' => 'post',
                    'id' => "editModalForm"
                    ])
                    !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                      {!! Form::label('name', 'Name', ['class'=>'col-md-2 control-label']) !!}
                      <div class="control-input">
                        {!! Form::text('name', null, array('class'=>'form-control',
                        'id'=>'chapter_name')) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <input type="hidden" name="course_id" id="chapter_course">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  {!! Form::label('description', 'Description', ['class'=>'col-md-2 control-label']) !!}
                  <div class="control-input">
                    {!! Form::textarea('description', null, array('class'=>'form-control',
                    'id'=>'chapter_description')) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>

<div class="modal fade" id="addSection" tabindex="-1" role="dialog" aria-labelledby="addSectionLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addSectionLabel">Add Section</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('SectionController@store'),
                    'method' => 'post'
                    ])
                    !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                      {!! Form::label('name', 'Name', ['class'=>'col-md-2 control-label']) !!}
                      <div class="control-input">
                        {!! Form::text('name', null, array('class'=>'form-control')) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <input type="hidden" name="chapter_id" id="chapter_id">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  {!! Form::label('description', 'Description') !!}
                  <div class="control-input">
                    {!! Form::textarea('description', null, array('class'=>'form-control')) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    {!! Form::close() !!}
    </div>
  </div>
</div>



<div class="modal fade" id="sectionModal" tabindex="-1" role="dialog" aria-labelledby="sectionModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Section</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => '',
                    'method' => 'post',
                    'id' => "editSectionForm"
                    ])
                    !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Name', ['class'=>'col-md-2 control-label']) !!}
                        <div class="control-input">
                        {!! Form::text('name', null, array('class'=>'form-control',
                        'id'=>'section_name')) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                <input type="hidden" name="chapter_id" id="section_chapter">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('description', 'Description', ['class'=>'col-md-2 control-label']) !!}
                    <div class="control-input">
                    {!! Form::textarea('description', null, array('class'=>'form-control',
                    'id'=>'section_description')) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    {!! Form::close() !!}
    </div>
  </div>
</div>


<div class="modal fade" id="addContentpdf" tabindex="-1" role="dialog" aria-labelledby="addContentpdfLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@pdfstore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="PDF">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Message',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addContentvideo" tabindex="-1" role="dialog" aria-labelledby="addContentvideoLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@videostore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="Video">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Message',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addContenthtml" tabindex="-1" role="dialog" aria-labelledby="addContentvideoLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@htmlstore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="Html/Video">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Message',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addContentquiz" tabindex="-1" role="dialog" aria-labelledby="addContentquizLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@quizstore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="Quiz">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Message',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>  
@endsection -->
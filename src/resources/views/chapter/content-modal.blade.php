@section('content-modal')
    
    @if(count($chapters) ==! 0)
    @if(count($chapter->sections) ==! 0)
    @include('chapter.modal')
    @endif
    @endif

 <div class="modal fade" id="addChapter" tabindex="-1" role="dialog" aria-labelledby="addChapterLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addChapterLabel">Add Chapter</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('ChapterController@store'),
                    'method' => 'post'
                    ])
                    !!}

                     @include('form.text', [
                        'field' => 'name',
                        'label' => 'Name',
                        'placeholder' => 'Name',
                        'default' => ''
                    ])


                    @include('form.textarea', [
                        'field' => 'description',
                        'label' => 'Description',
                        'placeholder' => 'Description',
                        'default' => ''
                    ])

                <input type="hidden" name="course_id" id="course_id">

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>


<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Chapter</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => '',
                    'method' => 'post',
                    'id' => "editModalForm"
                    ])
                    !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                      {!! Form::label('name', 'Name', ['class'=>'control-label']) !!}
                      <div class="control-input">
                        {!! Form::text('name', null, array('class'=>'form-control',
                        'id'=>'chapter_name')) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <input type="hidden" name="course_id" id="chapter_course">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  {!! Form::label('description', 'Description', ['class'=>'control-label']) !!}
                  <div class="control-input">
                    {!! Form::textarea('description', null, array('class'=>'form-control',
                    'id'=>'chapter_description')) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>

<div class="modal fade" id="addSection" tabindex="-1" role="dialog" aria-labelledby="addSectionLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addSectionLabel">Add Section</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('SectionController@store'),
                    'method' => 'post'
                    ])
                    !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                      {!! Form::label('name', 'Name', ['class'=>'control-label']) !!}
                      <div class="control-input">
                        {!! Form::text('name', null, array('class'=>'form-control', 'placeholder'=>'section name')) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <input type="hidden" name="chapter_id" id="chapter_id">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  {!! Form::label('description', 'Description') !!}
                  <div class="control-input">
                    {!! Form::textarea('description', null, array('class'=>'form-control', 'placeholder'=>'description')) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    {!! Form::close() !!}
    </div>
  </div>
</div>



<div class="modal fade" id="sectionModal" tabindex="-1" role="dialog" aria-labelledby="sectionModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Section</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => '',
                    'method' => 'post',
                    'id' => "editSectionForm"
                    ])
                    !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Name', ['class'=>'control-label']) !!}
                        <div class="control-input">
                        {!! Form::text('name', null, array('class'=>'form-control',
                        'id'=>'section_name')) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                <input type="hidden" name="chapter_id" id="section_chapter">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('description', 'Description', ['class'=>'control-label']) !!}
                    <div class="control-input">
                    {!! Form::textarea('description', null, array('class'=>'form-control',
                    'id'=>'section_description')) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    {!! Form::close() !!}
    </div>
  </div>
</div>
 






<div class="modal fade" id="addContentpdf" tabindex="-1" role="dialog" aria-labelledby="addContentpdfLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add PDF Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@pdfstore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="PDF">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addContentvideo" tabindex="-1" role="dialog" aria-labelledby="addContentvideoLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Video Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@videostore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="Video">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addContenthtml" tabindex="-1" role="dialog" aria-labelledby="addContentvideoLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Html Video Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@htmlstore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="Html/Video">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addContentquiz" tabindex="-1" role="dialog" aria-labelledby="addContentquizLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Quiz Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@quizstore'),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="Quiz">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div> 

<div class="modal fade" id="existing" tabindex="-1" role="dialog" aria-labelledby="addContentvideoLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Existing Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@existing'),
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        <input type="hidden" name="section_id" id="section_id" class="section_id">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.picklist', [
            'field' => 'content_id',
            'label' => 'Content',
            'placeholder' => 'Content',
            'name'  => 'content',
        ])
       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div> 

<div class="modal fade" id="sectionContentModal" tabindex="-1" role="dialog" aria-labelledby="sectionModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Content</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            {!!
                Form::open([
                    'role' => 'form',
                    'url' => '',
                    'method' => 'post',
                    'id' => 'action'
                    ])
                    !!}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Name', ['class'=>'control-label']) !!}
                        <div class="control-input">
                        {!! Form::text('name', null, array('class'=>'form-control',
                        'id'=>'section_content_name')) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                <input type="hidden" name="section_id" id="section">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('description', 'Description', ['class'=>'control-label']) !!}
                    <div class="control-input">
                    {!! Form::textarea('description', null, array('class'=>'form-control',
                    'id'=>'section_content_description')) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                @include('form.picklist', [
                    'field' => 'content_id',
                    'label' => 'Content',
                    'placeholder' => 'Content',
                    'name'  => 'editcontent',
                    'hidden->value' => ''
                ])

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
    {!! Form::close() !!}
    </div>
  </div>
</div>

@include('chapter.modal_list', [
    'name' => 'content',
    'title' => 'Content List',
    'placeholder' => 'find content by name',
])

@include('chapter.modal_list', [
    'name' => 'editcontent',
    'title' => 'Content List',
    'placeholder' => 'find content by name',
])


@endsection
@extends('master', ['active' => 'chapter'])
@section('sidebar')
    @include('chapter.sidebar', ['active' => 'add'])
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Add New Chapter</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/chapter') }}"> Back</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('ChapterController@store'),
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Name',
                'placeholder' => 'Name',
                'default' => ''
            ])

            @include('form.select', [
                'field' => 'course_id',
                'label' => 'Course',
                'options' => 
                    [''=>'']+App\Course::pluck('name','id')->all(), null,              
                'default' => ''
            ])

            @include('form.textarea', [
                'field' => 'description',
                'label' => 'Description',
                'placeholder' => 'Description',
                'default' => ''
            ])

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

            {!! Form::close() !!}


@endsection

@section('content-js')
    <script type="text/javascript">
    $(document).ready(function() {
      $(".js-example-basic-single").select2();
    });
    </script>
@endsection
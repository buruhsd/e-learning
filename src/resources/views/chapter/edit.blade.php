@extends('master', ['active' => 'chapter'])
@section('sidebar')
    @include('chapter.sidebar', ['active' => 'chapter'])
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Edit Chapter</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/admin/chapter') }}"> Back</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('ChapterController@update', [$chapter->id]),
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Name',
                'placeholder' => 'Name',
                'default' => $chapter->name
            ])

            @include('form.select', [
                'field' => 'course_id',
                'label' => 'Course',
                'options' => 
                    [$chapter->course->id=>$chapter->course->name]+App\Course::pluck('name','id')->all(), null,              
                'default' =>  $chapter->course->name,
                'hidden-value' => $chapter->course->id
            ])

            @include('form.textarea', [
                'field' => 'description',
                'label' => 'Description',
                'placeholder' => 'Description',
                'default' => $chapter->description
            ])

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

            {!! Form::close() !!}


@endsection


<div class="modal fade" id="addContent" tabindex="-1" role="dialog" aria-labelledby="addContentLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Content</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-sm-6" style="margin-bottom: 20px">
                <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $section->id }}" data-target="#addNewContent">Add New</a>
            </div>
            <div class="col-sm-6">
                <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $section->id }}" data-target="#existing">Existing</a>
            </div>
        </div>        
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="addNewContent" tabindex="-1" role="dialog" aria-labelledby="addContentLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Add Content</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-sm-6" style="margin-bottom: 20px">
                <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $section->id }}" data-target="#addContentpdf">PDF Content</a>
            </div>
            <div class="col-sm-6">
                <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $section->id }}" data-target="#addContentvideo">  Video Content</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6" style="margin-bottom: 20px">
                <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $section->id }}" data-target="#addContenthtml">  Html/Video Content</a>
            </div>
            <div class="col-sm-6">
                 <a type="button" class="btn btn-default btn-lg col-sm-12 text-center" style="padding: 50px;" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $section->id }}" data-target="#addContentquiz">  Quiz Content</a>
            </div>
        </div>        
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>


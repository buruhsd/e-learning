
<a class="list-group-item {{ isset($active) && $active == 'course' ? 'active' : '' }}" href="{{ url('/admin/course') }}">
	List Course
</a>

<a class="list-group-item {{ isset($active) && $active == 'add' ? 'active' : '' }}" href="{{ url('/admin/course/add') }}">
	Add Course
</a>

<div class="separate-sidebar"></div>

<a class="list-group-item {{ isset($active) && $active == 'trash' ? 'active' : '' }}" href="{{ url('/admin/course/trash') }}">
	Course on Trash List
</a>
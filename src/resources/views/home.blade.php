@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-16 col-sm-16">
              @role('admin')
                <div class="panel-group col-md-4 col-sm-16 col-xs-12">
                    <div class="panel panel-primary">
                      <div class="panel-heading"><h4><i class="glyphicon glyphicon-user"></i>&nbsp;Total Member:</h4>
                      </div>
                      <div class="panel-body"><strong>{{ $user }}</strong></div>
                    </div>
                </div>

                <div class="panel-group col-md-4 col-sm-16 col-xs-12">
                    <div class="panel panel-success">
                      <div class="panel-heading"><h4><i class="glyphicon glyphicon-user"></i>&nbsp; Total Teacher:</h4>
                      </div>
                      <div class="panel-body"><strong>{{ $teacher }}</strong></div>
                    </div>
                </div>
              @endrole
                <div class="panel-group col-md-4 col-sm-16 col-xs-12">
                    <div class="panel panel-warning">
                      <div class="panel-heading"><h4><i class="glyphicon glyphicon-list"></i>&nbsp;Total Course:</h4>
                      </div>
                      <div class="panel-body"><strong>{{ $course }}</strong></div>
                    </div>
                </div>

        </div>
    </div>
</div>
@endsection

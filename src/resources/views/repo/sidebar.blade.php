
<a class="list-group-item {{ isset($active) && $active == 'repo' ? 'active' : '' }}" href="{{ url('/admin/repo') }}">
	Repo List
</a>


<a class="list-group-item {{ isset($active) && $active == 'add' ? 'active' : '' }}" href="{{ url('/admin/repo/add') }}">
	Add Repo
</a>
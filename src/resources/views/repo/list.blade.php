@extends('master', ['active' => 'repo'])
@section('sidebar')
 	@include('repo.sidebar', ['active' => 'repo'])
@endsection
@section('content')
<div class="container-fluid">
    <div class="col-xs-12">
<div class="row" style="margin-left: -30px">
        {!!
                Form::open(array(
                    'class' => 'form-signin',
                    'role' => 'form',
                    'url' => action('RepoController@index'),
                    'method' => 'get',
                ))
            !!}
            <div class="col-md-2">
                @include('form.option', [
                'field' => 'category',
                'label' => 'Category',
                'options' => 
                [
                ''=>'All Category',
                'PDF' => 'PDF',
                'Video' => 'Video',
                'Html/Video' => 'Html/Video',
                'Quiz' => 'Quiz'
                ], null,
                'default' => (request()->input('category') ? request()->input('category') : 'ALL'),
                ])
            </div>
            <div class="col-md-4">
                @include('form.text', [
                    'label' => 'Find by',
                    'field' => 'search',
                    'placeholder' => "Search with name",
                    'default' => (request()->input('search') ? request()->input('search') : ''),
                ])
            </div>

            <div class="col-md-2" style="margin-top : 25px">
                <button type="submit" class="btn btn-primary">Search</button>
                {!! Form::close() !!}
            </div>
</div>
    </div>
	<div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($contents) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($contents as $key => $content)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $content->name }}</td>
                                <td>{{ $content->type }}</td>
                                <td>{{ $content->category }}</td>
                                <td>{{ $content->description }}</td>

                                <td>
                                
                                <a class="btn btn-xs btn-primary btn-show" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-id="{{ $content->id }}" data-name="{{ $content->name }}" data-description="{{$content->description}}" data-href="{{ action('RepoController@update', [$content->id]) }}" data-target="#show">edit</a>

                                @if($content->category == 'PDF')
                                    <a class="btn btn-xs btn-danger" href="{{ action('ContentController@deletepdf', [$content->id]) }}" onclick="return confirm('Are you sure you want to delete this item?')">remove</a>
                                    @elseif($content->category == 'Video')
                                    <a class="btn btn-xs btn-danger" href="{{ action('ContentController@deletevideo', [$content->id]) }}" onclick="return confirm('Are you sure you want to delete this item?')">remove</a>
                                    @elseif($content->category == 'Html/Video')
                                    <a class="btn btn-xs btn-danger" href="{{ action('ContentController@deletehtml', [$content->id]) }}" onclick="return confirm('Are you sure you want to delete this item?')">remove</a>
                                    @elseif($content->category == 'Quiz')
                                    <a class="btn btn-xs btn-danger" href="{{ action('ContentController@deletequiz', [$content->id]) }}" onclick="return confirm('Are you sure you want to delete this item?')">remove</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                
            </div>
            <div class="pull-right">
                {!! $contents->render() !!}
            </div>
        </div>
    </div>
</div>



@endsection

@section('content-modal')

<div class="modal fade" id="show" tabindex="-1" role="dialog" aria-labelledby="addContentpdfLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addContentLabel">Edit Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => '',
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post',
                'id' => 'update'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'attributes' => [
                'id' => 'name'
            ]

        ])        

        <input type="hidden" name="section_id" id="name" class="section_id">
        <input type="hidden" name="category" id="category" class="category" value="PDF">
       
        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'id' => 'description'
            ],
        ])
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

@endsection

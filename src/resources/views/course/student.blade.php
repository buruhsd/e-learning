@extends('master', ['active' => 'course'])
@section('sidebar')
 	@include('course.sidebar', ['active' => 'course'])
@endsection
@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Course</p>
                    <p class="medium-title"><small>{{$course->name}}</small></p>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Subject</p>
                    <p class="medium-title">{{$course->subject->name}}</p>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Ladder</p>
                    <p class="medium-title">{{$course->ladder->name}}</p>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>Students</p>
                    <p class="medium-title">{{$count}}</p>
                </div>
            </div>
        </div>
    </div>

<div class="col-xs-12">
<div class="row" style="margin-left: -30px">
        {!!
                Form::open(array(
                    'class' => 'form-signin',
                    'role' => 'form',
                    'url' => action('CourseController@mystudent', $course->id),
                    'method' => 'get',
                ))
            !!}
            
            <div class="col-md-4">
                @include('form.text', [
                    'label' => 'Find by',
                    'field' => 'search',
                    'placeholder' => "Search with name",
                    'default' => (request()->input('search') ? request()->input('search') : ''),
                ])
            </div>

            <div class="col-md-2" style="margin-top : 25px">
                <button type="submit" class="btn btn-primary">Search</button>
                {!! Form::close() !!}
            </div>
</div>
</div>
	<div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($students) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($students as $key => $course)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $course->name }}</td>
                                <td>
                                    <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                    
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                
            </div>
            <div class="pull-right">
            </div>
        </div>
    </div>


</div>

@endsection
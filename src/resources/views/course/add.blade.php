@extends('master', ['active' => 'course'])
@section('sidebar')
    @include('course.sidebar', ['active' => 'add'])
@endsection

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Add New Course</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/course') }}"> Back</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif


    {!!
                Form::open([
                    'role' => 'form',
                    'url' => action('CourseController@store'),
                    'method' => 'post'
                ])
            !!}

            @include('form.text', [
                'field' => 'name',
                'label' => 'Name',
                'placeholder' => 'Name',
                'default' => ''
            ])

            
            @include('form.select', [
                'field' => 'subject_id',
                'label' => 'Subject',
                'options' => 
                    [''=>'Select Subject']+App\Subject::where('status', '=', 'active')->pluck('name','id')->all(), null,              
                'default' => '',
                'placeholder' => 'Select Subject',
            ])

            @include('form.select', [
                'field' => 'ladder_id',
                'label' => 'Ladder',
                'options' => 
                    [''=>'Select Ladder']+App\Ladder::where('status', '=', 'active')->pluck('name','id')->all(), null,              
                'default' => '',
                'placeholder' => 'Select Ladder'
            ])

            @if (Auth::user()->role=='admin')

            @include('form.select', [
                'field' => 'teacher_id',
                'label' => 'Teacher',
                'options' => 
                    [''=>'Select Teacher']+App\User::where('role', '=', 'teacher')->pluck('name','id')->all(), null,              
                'default' => '',
                'placeholder' => 'Select teacher'
            ])

            @else

            <input class="form-control" type="hidden" name="teacher_id" value={{ Auth::user()->id }}>
            
            @endif

            @include('form.textarea', [
                'field' => 'description',
                'label' => 'Description',
                'placeholder' => 'Description',
                'default' => ''
            ])

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

            {!! Form::close() !!}


@endsection

@section('content-modal')   
    @include('subject.modal_picklist', [
        'name' => 'subject',
        'title' => 'SUbject List',
        'placeholder' => 'find subject by name',
    ])

    @include('ladder.modal_picklist', [
        'name' => 'ladder',
        'title' => 'ladder List',
        'placeholder' => 'find ladder by name',
    ])
@endsection

@section('content-js')
    <script>
    $(function () {
        CreatePicklist('subject', '/subject/list?');
        CreatePicklist('ladder', '/ladder/list?');
    });
    </script>
@endsection
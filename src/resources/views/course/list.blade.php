@extends('master', ['active' => 'course'])
@section('sidebar')
 	@include('course.sidebar', ['active' => 'course'])
@endsection
@section('content')

<div class="container-fluid">
<div class="col-xs-12">
<div class="row" style="margin-left: -30px">
        {!!
                Form::open(array(
                    'class' => 'form-signin',
                    'role' => 'form',
                    'url' => action('CourseController@index'),
                    'method' => 'get',
                ))
            !!}
            <div class="col-md-2">
                @include('form.option', [
                'field' => 'subject',
                'label' => 'Subject',
                'options' => [''=>'All Subject']+App\Subject::pluck('name','id')->all(), null,
                'default' => (request()->input('subject') ? request()->input('subject') : 'ALL'),
                ])
            </div>
            <div class="col-md-4">
                @include('form.text', [
                    'label' => 'Find by',
                    'field' => 'search',
                    'placeholder' => "Search with name",
                    'default' => (request()->input('search') ? request()->input('search') : ''),
                ])
            </div>

            <div class="col-md-2" style="margin-top : 25px">
                <button type="submit" class="btn btn-primary">Search</button>
                {!! Form::close() !!}
            </div>
</div>
</div>
        <div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Ladder</th>
                            <th>Subject</th>
                            <th>Teacher</th>
                            <th>Students List</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($courses) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($courses as $key => $course)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $course->name }}</td>
                                <td>{{ $course->ladder->name }}</td>
                                <td>{{ $course->subject->name }}</td>
                                <td>{{ $course->teacher->name }}</td>
                                <td>
                                <a class="btn-xs btn-success btn" href="{{ action('CourseController@mystudent', $course->id) }}">show</a>
                                </td>

                                <td>
                                    <a class="btn btn-primary btn-xs" href="{{ action('CourseController@edit', $course->id) }}">edit</a>
                                    <a class="btn btn-primary btn-xs" href="{{ action('ChapterController@chaptermanager', $course->id) }}">course manager</a>
                                    <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                    <a class="btn btn-xs btn-danger" href="{{ action('CourseController@softdelete', $course->id) }}" onclick="return confirm('Are you sure you want to move this item to trash?')">delete</a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                
            </div>
            <div class="pull-right">
                {!! $courses->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection

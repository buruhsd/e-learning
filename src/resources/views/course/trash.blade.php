@extends('master', ['active' => 'course'])
@section('sidebar')
 	@include('course.sidebar', ['active' => 'trash'])
@endsection
@section('content')

<div class="container-fluid">
    <div class="col-xs-12"> 
    @include('form.search',['url'=>'course','link'=>'course'])
    </div>
   
        <div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Ladder</th>
                            <th>Subject</th>
                            <th>Teacher</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($course) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($course as $key => $course)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $course->name }}</td>
                                <td>{{ $course->ladder->name }}</td>
                                <td>{{ $course->subject->name }}</td>
                                <td>{{ $course->teacher->name }}</td>

                                <td>
                                    <a class="btn btn-primary btn-xs" href="{{ action('CourseController@restore', $course->id) }}">restore</a>
                                    <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                    <a class="btn btn-xs btn-danger" href="{{ action('CourseController@delete', $course->id) }}" onclick="return confirm('Are you sure you want to delete this item?')">delete</a>
                                    
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                
            </div>
            <div class="pull-right">
                
            </div>
        </div>
    </div>
</div>
@endsection

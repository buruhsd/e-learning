@extends('master', ['active' => 'storage'])

@section('sidebar')
	@include('storage.storage.sidebar', ['active' => 'edit'])
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="content-header">
				<h2>
					Edit Storage
				</h2>
			</div>
		</div>
		<div class="row">
			{!!
				Form::open([
					'role' => 'form',
					'url' => action('Storage\StorageController@update', [$storage->id]),
					'method' => 'post'
				])
			!!}
			
			@include('form.text', [
				'field' => 'name',
				'label' => 'Storage Name',
				'placeholder' => 'Storage Name',
				'default' => $storage->name
			])

			<div class="form-group">
				<label for="transfer_mode" class="control-label">Type</label>
				<div class="control-input {{ $errors->has('transfer_mode') ? 'has-error' : '' }}">
					<select class="form-control" name="transfer_mode">
						<option value="sme" @if($storage->transfer_mode == 'sme') selected @endif>Storage</option>
						<option value="ltl" @if($storage->transfer_mode == 'ltl') selected @endif>Store</option>
					</select>
					@if ($errors->has('transfer_mode'))
					<span class="help-block text-danger">{{ $errors->first('transfer_mode') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label for="is_active" class="control-label">Status</label>
				<div class="control-input {{ $errors->has('is_active') ? 'has-error' : '' }}">
					<select class="form-control" name="is_active">
						<option value="true" @if($storage->is_active == true) selected @endif>Active</option>
						<option value="false" @if($storage->is_active == false) selected @endif>Inactive</option>
					</select>
					@if ($errors->has('is_active'))
					<span class="help-block text-danger">{{ $errors->first('is_active') }}</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

			{!! Form::close() !!}				
		</div>
	</div>
@endsection

@section('content-js')
@endsection
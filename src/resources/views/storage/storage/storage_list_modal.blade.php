<div id="filterState" class="text-right">
	<div class="pagination" style="margin: 10px 16px 0 0;">
		@if ($state == 'all')
		<a href="{{ action('Storage\StorageController@picklist') }}" class="pull-right">
			Show Active Storages
		</a> 
		@else
		<a href="{{ action('Storage\StorageController@picklist', ['state' => 'all']) }}" class="pull-right">
			Show All
		</a> 
		@endif
	</div>
</div>
<table class="table">
	<thead>
	  <tr>
		<th>Storages Name</th>
		<th>Type</th>
		<th>Select</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($storages as $key => $storage)
			<tr class="{{ $storage->is_active == 0 ? 'active' : '' }}">
				<td>{{ $storage->name }}</td>
				<td>{{ $storage->transfer_mode == 'ltl' ? 'Store' : 'Storage' }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-warning btn-xs btn-choose"
						type="button" data-id="{{ $storage->id }}" data-name="{{ $storage->name }}">
						Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $storages->appends(Request::except('page'))->render() !!}

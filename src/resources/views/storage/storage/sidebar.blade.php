@if(isset($active) && ($active == 'index' || $active == 'create'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Storage\StorageController@index') }}">
	Storage List
</a>
<a class="list-group-item {{ isset($active) && $active == 'create' ? 'active' : '' }}" href="{{ action('Storage\StorageController@create') }}">
	Create Storage
</a>
@elseif(isset($active) && ($active == 'index' || $active == 'edit'))
<a class="list-group-item {{ isset($active) && $active == 'index' ? 'active' : '' }}" href="{{ action('Storage\StorageController@index') }}">
	Storage List
</a>
<a class="list-group-item {{ isset($active) && $active == 'edit' ? 'active' : '' }}" href="{{ action('Storage\StorageController@edit', [$storage->id]) }}">
	Edit Storage
</a>
@endif
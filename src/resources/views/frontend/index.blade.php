@extends('layouts.frontend')
@section('content-css')
<style media="screen">

</style>
@endsection
@section('header')
<section id="header">
	<!-- Full Page Image Background Carousel Header -->
	<header id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			@foreach ($sliders as $key => $slider)
			<li data-target="#myCarousel" data-slide-to="{{$key}}" class=" {{$key == 0 ? 'active':''}}"></li>
			@endforeach
		</ol>
		<!-- Wrapper for Slides -->
		<div class="carousel-inner">


			@foreach ($sliders as $key => $slider)
				<div class="item {{$key == 0 ? 'active':''}}">
					<!-- Set the third background image using inline CSS below. -->
					<div class="fill" style="background-image:url('{{ URL::route('slider_image', $slider->image) }} ');"></div>
					<div class="carousel-caption">
					</div>
				</div>
			@endforeach


		</div>
	</header>
</section>
@endsection
@section('landing')
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="title text-center">
				<h3>Choose your subject to study now </h3>
				<p>A Collection of Interactive Videos, Lesson Notes and Practice Questions</p>
			</div>
		</div>
	</div>
	{{-- kelas pilihan --}}
	<div class="clearfix"></div>
	<div class="underlined-title text-center">
		<h3>Featured Subjects</h3>
		<hr>
	</div>
	<div class="row">
		<div class="col-sm-6 featured-class">
			<?php
			$featured_id_1 = config('settings.featured_subject_1');
			$featured_1 = \App\Subject::find($featured_id_1);
			?>
			<a href="{{ url('subject/'.$featured_1->id)}}">
				<div style="background: url({{ url('/images/'.$featured_1->images)}});height: 350px;background-size: cover;">
					<div class="course">
						<p>{{ $featured_1->name }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-6 featured-class">
			<?php
			$featured_id_2 = config('settings.featured_subject_2');
			$featured_2 = \App\Subject::find($featured_id_2);
			?>
			<a href="{{ url('subject/'.$featured_2->id)}}">
				<div style="background: url({{ url('/images/'.$featured_2->images)}});height: 350px;background-size: cover;">
					<div class="course">
						<p>{{ $featured_2->name }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-6 featured-class">
			<?php
			$featured_id_3 = config('settings.featured_subject_3');
			$featured_3 = \App\Subject::find($featured_id_3);
			?>
			<a href="{{ url('subject/'.$featured_3->id)}}">
				<div style="background: url({{ url('/images/'.$featured_3->images)}});height: 350px;background-size: cover;">
					<div class="course">
						<p>{{ $featured_3->name }}</p>
					</div>
				</div>
			</a>
		</div>
		<div class="col-sm-6 featured-class">
			<?php
			$featured_id_4 = config('settings.featured_subject_4');
			$featured_4 = \App\Subject::find($featured_id_4);
			?>
			<a href="{{ url('subject/'.$featured_4->id)}}">
				<div style="background: url({{ url('/images/'.$featured_4->images)}});height: 350px;background-size: cover;">
					<div class="course">
						<p>{{ $featured_4->name }}</p>
					</div>
				</div>
			</a>
		</div>
	</div>
	<hr>
	<div class="text-center">
		<a class="btn btn-custom btn-lg" href="{{ url('/subjects') }}">View More</a>
	</div>
	{{-- pelajaran pilihan --}}
	{{-- <div class="underlined-title text-center">
		<h3>Pelajaran Pilihan</h3>
		<hr>
	</div>
	<div class="row">
		<div class="col-sm-3 featured-subject">
			<div>
				<img src="/images/math.png" class="img-responsive">
			</div>
		</div>
		<div class="col-sm-3 featured-subject">
			<div>
				<img src="/images/math.png" class="img-responsive">
			</div>
		</div>
		<div class="col-sm-3 featured-subject">
			<div>
				<img src="/images/math.png" class="img-responsive">
			</div>
		</div>
		<div class="col-sm-3 featured-subject">
			<div>
				<img src="/images/math.png" class="img-responsive">
			</div>
		</div>
	</div> --}}
</div>
<br><br>
<div id="strip" class="strip">
	<div class="container">
		<div class="inner-strip">
			<div class="row">
				<div class="col-md-12">
					<div class="col-sm-6 no-padding">
						<h3>Free opportunity for everyone</h3>
						<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>-->
					</div>
					<div class="col-sm-6 text-right no-padding">
						<br>
						@if (!Auth::check())
						<a href="{{ url('/login') }}" class="btn btn-custom btn-lg">GET STARTED</a>
            @endif
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
{{-- advantage --}}
<div class="container">
	{{-- pelajaran pilihan --}}
	<div class="underlined-title text-center">
		<h3>Why Semesta Web TV</h3>
		<hr>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-6 advantage">
			<img src="../images/Asset 2.png" width="70px" class="img-responsive">
			<h3 class="green-text">User Friendly Web Page.</h3>
			<p>Tujuan kami membuka Semestaweb.tv adalah untuk menyampaikan pendidikan berkualitas secara mudah dan sederhana kepada semua orang. Untuk itu, kami berusaha untuk membentuk halaman web kami sesederhana dan sepraktis mungkin. Kami mendesain halaman web kami dalam bentuk sederhana yang akan mengarahkan anda dengan cepat dan ringkas, serta menghindari menu yang padat dan sulit dimengerti.</p>
		</div>
		<div class="col-sm-12 col-md-6 advantage">
			<div id="advantage-1" class="embed-responsive embed-responsive-16by9">

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-sm-12 col-md-6 advantage">
			<div id="advantage-2" class="embed-responsive embed-responsive-16by9">

			</div>
		</div>
		<div class="col-sm-12 col-md-6 advantage">
			<img src="../images/Asset 3.png" width="70px" class="img-responsive">
			<h3 class="green-text">Save 5 min to Learn.</h3>
			<p>Menonton penjelasan pelajaran di layar komputer membutuhkan konsentrasi dan perhatian lebih. Perlu diketahui bahwa siswa lebih sulit berkonsentrasi dalam mendengarkan video pelajaran berdurasi 40 menit dibandingkan mendengarkan langsung di kelas. Oleh karena itu, video-video semestaweb.tv disiapkan dengan durasi optimum 5 menit. Dalam durasi 5 menit, pengisi materi mempersembahkan sebuah lesson plan yang bertujuan untuk membantu siswa memahami konsep dan kompetensi dasar materi.</p>
		</div>
		<div class="clearfix"></div>
		<div class="col-sm-12 col-md-6 advantage">
			<img src="../images/Asset 4.png" width="70px" class="img-responsive">
			<h3 class="green-text">Interactive Videos.</h3>
			<p>Memperhatikan reaksi orang-orang terhadap perkembangan teknologi merupakan suatu hal yang penting. Perlu diketahui bahwa siswa lebih sulit berkonsentrasi dalam mendengarkan video pelajaran berdurasi 40 menit dibandingkan mendengarkan langsung di kelas. Agar siswa bisa berkonsentrasi terhadap materi, video-video di semestaweb.tv diputar dengan video player khusus. Video player ini memungkinkan kami untuk menyiapkan video yang bersifat interaktif. Video interaktif akan membantu siswa berkonsentrasi dengan cara mengarahkan mereka kepada soal-soal yang berkaitan dengan kompetensi dasar materi. Ini merupakan ciri khas  video-video di semestaweb.tv.</p>
		</div>
		<div class="col-sm-12 col-md-6 advantage">
			<div id="advantage-3" class="embed-responsive embed-responsive-16by9">

			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-sm-12 col-md-6 advantage">
			<div  id="advantage-4" class="embed-responsive embed-responsive-16by9">

			</div>
		</div>
		<div class="col-sm-12 col-md-6 advantage">
			<img src="../images/Asset 5.png" width="70px" class="img-responsive">
			<h3 class="green-text">Interactive Practice Questions.</h3>
			<p>Ketika seorang siswa mendengarkan suatu materi, bukan berarti siswa tersebut memahami materi yang didengarnya. Apa yang dipahami oleh siswa perlu diuji dan kesalahan-kesalahannya perlu diperbaiki, dengan begitu proses belajar dapat terwujud. Selain mempersembahkankan video penjelasan materi, Semestaweb.tv juga menyediakan soal-soal evaluasi interaktif yang berkaitan dengan topik yang dijelaskan. Siswa dapat memanfaatkan fitur tersebut guna mengukur dan mengevaluasi apa saja yang telah ia pelajari. Menyediakan soal-soal latihan di samping penjelasan merupakan salah satu keunggulan kami. Selain itu, siswa dapat mengoreksi kesalahannya saat itu juga.</p>
		</div>
		<div class="clearfix"></div>
		<div class="col-sm-12 col-md-6 advantage">
			<img src="../images/Asset 6.png" width="70px" class="img-responsive">
			<h3 class="green-text">Lesson Notes</h3>
			<p>Perbedaan semestaweb.tv dari portal-portal yang lain adalah kami memberi kesempatan bagi siswa untuk mengunduh catatan penjelasan materi baik dalam bentuk digital maupun hard copy.  Catatan pelajaran ini dapat pula dijadikan buku catatan yang bisa selalu digunakan di dunia pendidikan.</p>
		</div>
		<div class="col-sm-12 col-md-6 advantage">
			<div id="advantage-5" class="embed-responsive embed-responsive-16by9">


			</div>
		</div>
	</div>
</div>
<div class="strip-2">
   <div class="container text-center">
      <div class="inner-strip" style="background: none;">
         <h3 class="white-text">DO NOT HESITATE TO CONTACT US FOR YOUR REQUEST</h3>
         <br>
         <a href="{{url('/contact-us')}}" target="_top" class="btn btn-custom btn-lg">CONTACT US</a>
      </div>

   </div>
</div>
@endsection
@section('content')
@endsection
@section('content-js')
<script type="text/javascript">
var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // Replace the 'ytplayer' element with an <iframe> and
  // YouTube player after the API code downloads.

	$(function(){
		var src_home = '/images/logo_semestawebtv.png';
		var src = '/images/logo_semestawebtv.png';
		$('.navbar-default').addClass('navbar-default-home');
		$('.navbar-nav').addClass('navbar-nav-home');
		$('.img-logo').attr('src', src_home);

			var baseHeight = $(window).height(); // for body, use $("body").height();
			var num = 1; // this the percentage of vertical scroll
			var action_num = .8;
			var h = $(window).height();
			var tcopt = ['#FFFFFF','#AAAAAA','#555555'];
			var tact = ['#000000','#5BC19A']

			$(window).on('scroll', function () {
				var sc = ($(this).scrollTop() > 0) ? $(this).scrollTop():1;
				var s = sc/h;
				$('.navbar').css('background-color','rgba(255,255,255,'+s+')')

				if(s>0.6){
					$('.navbar-nav').find('a').css('color',tcopt[2]);
					$('.navbar-nav').find('.active').css('color',tact[1]);
				}else if(s>0.3 && s<0.61){
						$('.navbar-nav').find('a').css('color',tcopt[1]);
						$('.navbar-nav').find('.active').css('color',tact[0]);
				}else if(s<0.3 ){
						$('.navbar-nav').find('a').css('color',tcopt[0]);
						$('.navbar-nav').find('.active').css('color',tact[0]);
				}else{

				}
				/*
				if ($(window).scrollTop() / baseHeight > action_num) {
					$('.navbar-default').removeClass('navbar-default-home');
					$('.navbar-nav').removeClass('navbar-nav-home');
					$('.img-logo').attr('src', src);
				} else {
					$('.navbar-default').addClass('navbar-default-home');
					$('.navbar-nav').addClass('navbar-nav-home');
					$('.img-logo').attr('src', src_home);
				}*/
			});
		})

		function loadyt(n){
			var yt_id = [
				'4OH8-bAGsQg',
				'HQciw8kHHkM',
				'NVTruTDe1OY',
				'MpdbwgUHVxU',
				'ZDN-H6U6p8g'
			]
			m = parseInt(n)+1;
			$('#advantage-'+m).html('<iframe style="display:none" class="ytplayer" type="text/html" class="embed-responsive-item"'+
'src="https://www.youtube.com/embed/'+yt_id[n]+'?autoplay=0&showinfo=0&controls=0&modestbranding=1&loop=1&rel=0&playlist='+yt_id[n]+'&color=pink"'+
'frameborder="0"></iframe>');
			$('#advantage-'+m).find('.ytplayer').ready(function() {

				//				$(this).show();
												$(this).find('.ytplayer').fadeIn( "slow", function() {
    // Animation complete
  });
				if((m)<yt_id.length){
					setTimeout(function(){
						loadyt(m)
					},800)
				}
			})
		}

		$( window ).on( "load", function() {
				loadyt(0);

		})

	</script>
	@endsection
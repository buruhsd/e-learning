	@extends('layouts.frontend')

	@section('content-css')
	<style media="screen">
		.lstcourse > .panel-primary {
			border-color: #06af8f;
			min-height: 11.3em;
		}
		.courseprev > h3 {
			color: #06af8f;
		}

		.nothing{
			height: 20vh;
    	color: #aaa;
		}

		.courseicon {
			color: rgba(252, 85, 85, 0.74);;
		}

		.courseprev > p {
			color: #5f5d5d;
		}


	</style>
	@endsection

	@section('content')
	<div class="content-margin-top" style="margin-bottom:40px">
		<div class="container-fluid" >
			<br><br>
			<ul class="breadcrumb-customs">
				<li><a href="{{ url('/') }}">Home</a></li> >
				<li><a href="{{ url('/subjects') }}">Subjects</a></li> >
				<li><a class="active">{{$subject->name}}</a></li>
			</ul>
			{{-- pelajaran pilihan --}}
			<div class="underlined-title title-content text-center">
				<h3>Learn {{$subject->name}}</h3>
				<hr>
			</div>
			<div class="col-sm-6 col-sm-offset-3 ">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="ytplayer embed-responsive-item" src="{{$subject->url}}?autoplay=0&showinfo=0&controls=0&modestbranding=1&rel=0" frameborder="0" ></iframe>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="col-sm-8 col-sm-offset-2">
			<p class="text-center">
				{{-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum. --}}
				{{ $subject->description }}
			</p>
			</div>
			<div class="clearfix"></div>
			<div class="course-heading text-center">
				<h3>Choose your course </h3>
				<br><br>
				<ul class="nav nav-tabs nav-justified tab-sign-in" role="tablist">
					@foreach ($ladders as $key=>$ladder)
					<li role="presentation" class="{{ $key==0 ? "active" : "" }}">
						<a href="#{{$ladder->id}}" aria-controls="sd" role="tab" data-toggle="tab">{{ $ladder->name }}</a>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="container">
			<div class="tab-content tab-content-customs">
				@foreach($ladders as $key=>$ladder)
				<div role="tabpanel" class="tab-pane fade in {{ $key==0 ? "active" : "" }}" id="{{$ladder->id}}">
					<?php $courses = $ladder->courses->where('subject_id', $id); ?>
					@if(count($courses)>0)
					@foreach($courses as $key=>$course)

						<a class="col-md-4 lstcourse" href="{{ url('/course/'.$course->id) }}">
							<div class="panel panel-primary">
							<div class="panel-body">
								<div class="col-md-3"><h1><i class="courseicon fa fa-graduation-cap fa-lg"></i></h1></div>
								<div class="col-md-9 courseprev">
									<h3>{{$course->name}}</h3><p>
								{{ str_limit($course->description, $limit = 75, $end = '...') }}
								</p>
								</div>

						</div>
						</div>
					</a>
					@endforeach
					@else
						<div class="nothing">
							<h1>Nothing found</h1>
						</div>
					@endif

					</div>

					@endforeach
				</div>
			</div>
			{{-- <div class="container">
				<div class="total-followers">
					<span class="fa fa-user"></span> Join 285,102 other students.
				</div>
			</div> --}}
			{{-- <div class="container">
				<div class="row">
					<div class="col-sm-6 col-xs-12 imagine-left">
						<h4>Imagine your future!</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
					</div>
					<div class="col-sm-6 col-xs-12 imagine-right">
						<a href="#" class="btn btn-custom-2 btn-lg">Start Course</a>
					</div>
				</div>
			</div> --}}
		</div>
	</div>
	@endsection
	@section('content-js')
	<script type="text/javascript">
	$(window).resize(function() {
		max9()
	})
	function max9(){
		if($('.tab-content-customs').height() > 550){
			$('.tab-content-customs').css('max-height','550px');
			$('.tab-content-customs').css('overflow-y','scroll');
		}
	}
		$(function(){
			max9()

			$('.chapter-list a').on('click', function(e){
				e.preventDefault();
				$(this).addClass('active').siblings().removeClass('active');
				id = $(this).data('chapter');

				$('.section-list').each(function(){
					if($(this).hasClass('chapter'+id)){
						$(this).addClass('show-section');
					}
					else{
						$(this).removeClass('show-section');
					}
				});
			});

			$('.product-list li a').click(function(e) {
				if($(this).hasClass('unclickable'))
					e.preventDefault();
			});
		})
	</script>
	@endsection

@extends('layouts.frontend')
@section('content-css')

<link rel="stylesheet" href="{{ elixir("css/contact-us.css") }}">

<style media="screen">
#message {
  position: fixed;
  top: 110px;

  /* left: 0; */
  margin-left: 2.5%;
  margin-right: 2.5%;
  width: 90%;
  z-index: 999;
  padding: 5px;
}
#inner-message {
    margin: 0 auto;
}
</style>
@endsection
@section('content')

<div class="container-margin-top" style="margin-top:120px">
  <div id="message" style="display: none;">
        <div id="inner-message" class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info-circle" aria-hidden="true"></i> <span id="message-body"></span>
        </div>
  </div>
    <div class="container">
      <!--contact header-->
      <div class="contact-header col-md-12 col-lg-12 com-sm-12">
          <h3>Contact Us</h3>
          <center>
            <div class="header-strip">
            </div>
          </center>
          <div class="contact-header-text">
            <p>
              We are looking forward to hearing from you
            </p>
            <p>
              Please fill 'Contact Us' form and we will get back to you soon
            </p>
          </div>
      </div>
      <!--contact header end-->
      <!--contact body-->
      <div class="contact-body col-md-12 col-sm-12">
        <!--Contact info-->
        <div class="contact-body-content col-md-6 col-sm-12">
          <h3>
            Contact Info
          </h3>
          <div class="">
            <p>
              But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.
            </p>
            <div class="col-md-12 col-xs-12 col-sm-12">
               <div class="social-contact">
                 <div class="col-xs-6">
                   <div class="col-sm-3 col-xs-12 social-icon">
                      <i class="green-text fa fa-envelope"></i>
                   </div>
                   <div class="col-sm-9 col-xs-12 social-text">
                      <p>{{config('settings.email')}}</p>
                   </div>
                 </div>
                  {{-- facebook --}}
                  <div class="col-xs-6">
                    <div class="col-sm-3 col-xs-12 social-icon">
                       <i class="green-text fa fa-facebook-square"></i>
                    </div>
                    <div class="col-sm-9 col-xs-12 social-text" style="margin-bottom: 10px;">
                       <a href="{{config('settings.facebook')}}">SemestaWebTv</a>
                    </div>
                  </div>

                  {{-- twitter --}}
                  <div class="col-xs-6">
                    <div class="col-sm-3 col-xs-12 social-icon">
                       <i class="green-text fa fa-twitter-square"></i>
                    </div>
                    <div class="col-sm-9 col-xs-12 social-text" style="margin-bottom: 10px;">
                       <a href="{{config('settings.twitter')}}" >@SemestaWebTv</a>
                    </div>
                  </div>
                  {{-- youtube --}}
                  <div class="col-xs-6">
                    <div class="col-sm-3 col-xs-12 social-icon">
                       <i class="green-text fa fa-youtube-square"></i>
                    </div>
                    <div class="col-sm-9 col-xs-12 social-text" style="margin-bottom: 10px;">
                       <a href="{{config('settings.youtube')}}" >SemestaWebTv</a>
                    </div>
                  </div>
                  {{-- twitter --}}
                  <div class="col-xs-6">
                    <div class="col-sm-3 col-xs-12 social-icon">
                       <i class="green-text fa fa-instagram"></i>
                    </div>
                    <div class="col-sm-9 col-xs-12 social-text" style="margin-bottom: 10px;">
                       <a href="{{config('settings.instagram')}}" >SemestaWebTv</a>
                    </div>
                  </div>
               </div>
            </div>
          </div>
        </div>
        <!--Contact info end-->
        <!--Contact form-->
        <div class="contact-body-content col-md-6 col-xs-12 col-sm-12">
          <h3>
            Leave Us Message
          </h3>
          <div class="">
            {!!
              Form::open([
                'role' => 'form',
                'url' => action('MessageController@store'),
                'method' => 'post'
                ])
                !!}
              <div id="gname" class="form-group">
                  <input type="text" class="form-control" name="name" id="txt_name" placeholder="Who are you?" >
                  <span id="helpBlockname" style="display:none" class="help-block"></span>
              </div>
              <div id="gemail" class="form-group">
                  <input type="text" class="form-control" name="email" id="txt_email" placeholder="Your email address please?" >
                  <span id="helpBlockemail" style="display:none" class="help-block">A block of help text that breaks onto a new line and may extend beyond one line.</span>
              </div>
              <div id="gmessage" class="form-group">
                  <textarea name="message" class="form-control" rows="6" cols="40" id="txt_message" placeholder="Your message"></textarea>
                  <span id="helpBlockmessage" style="display:none" class="help-block">A block of help text that breaks onto a new line and may extend beyond one line.</span>
              </div>
              <div class="form-group">
                <a id="btnsubmit" href="#" class="btn btn-custom"> SEND MESSAGE</a>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
        <!--Contact form end-->
      </div>
      <!--contact body end-->
    </div>
</div>
@endsection
@section('content-js')
<script>

  $('#btnsubmit').click(function(){
    var valid = true;
    if($('#txt_name').val().length < 2){
        $('#helpBlockname').html("Name is not valid").show();
        $('#gname').addClass('has-error');
        valid = false;
    }else{
      valid = true;
      $('#helpBlockname').hide();
      $('#gname').removeClass('has-error');
    }

    if($('#txt_email').val().length <2 ){
      $('#helpBlockemail').html("Email cannot be empty").show();
      $('#gemail').addClass('has-error');
      valid = false;
    }else{
      valid = true;
      $('#helpBlockemail').hide();
      $('#gemail').removeClass('has-error');
    }
    if($('#txt_message').val().length < 2){
      $('#helpBlockmessage').html("Message cannot be empty").show();
      $('#gmessage').addClass('has-error');
      valid = false;
    }else{
      $('#helpBlockmessage').hide();
      $('#gmessage').removeClass('has-error');
      valid = true;
    }
    if(valid){
      $('.form-group').removeClass('has-error');
      $('.help-block').hide();
      $.ajax
       ({
           type: "POST",
           //the url where you want to sent the userName and password to
           url: "{{ URL::route('postmessage') }}",
           dataType: 'json',
           async: false,
           //json object to sent to the authentication url
           data: JSON.stringify({ "name": $('#txt_name').val() , "email": $('#txt_email').val(),"message": $('#txt_message').val() }),
           success: function (o) {
             console.log(o);
             $('#txt_name').val("");
             $('#txt_email').val("");
             $('#txt_message').val("");

             $('#message').html('<div id="message">'+
                   '<div id="inner-message" class="alert alert-'+o.status+'">'+
                       '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                       '<i class="fa fa-info-circle" aria-hidden="true"></i> '+ o.message +
                   '</div>'+
             '</div>').show();
             setTimeout(function(){
               $('#message').fadeOut("slow",function(){});
             },2000);
           }
       });
    }

  })
</script>
@endsection

@extends('layouts.frontend')
@section('content-css')
<style media="screen">
	.about-header{
		padding-top: 50px;
		height: 170px;
	}
	.content-about{
		margin-bottom: 10vh;
	}
</style>
@endsection
@section('content')
<div class="content-margin-top about-header">
		<h1>About Us</h1>
</div>
<div class="content-about">
	<div class="container">
		<div class="underlined-title text-center">
			<h3>Our Mission</h3>
			<hr>
		</div>
		<div class="row">
			<div class="col-md-7 col-sm-8">
				<p>Our mission is to bring accessible. affordable, engaging, and hoghly effective higher education to the world. We believe that higher education is basic human right, and we seek to empower our students to advance their education and careers.</p><br>
				<p>Education is no longer a one-time event but a lifelong experience. Education should be less passive listening (no long lectures) and more active doing Education should.</p>
			</div>
			<div class="col-md-5 col-sm-4">
				<div  id="about-1" class="embed-responsive embed-responsive-16by9">

				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="underlined-title text-center">
			<h3>Our Story &  Target</h3>
			<hr>
		</div>
		<div class="row">
			<div class="col-md-5 col-sm-4">
				<div  id="about-2" class="embed-responsive embed-responsive-16by9">

				</div>
			</div>
			<div class="col-md-7 col-sm-8">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="underlined-title text-center">
				<h3>Our Staff</h3>
				<hr>
			</div>
			<div class="row">
				<div class="col-md-7 col-sm-8">
					<p>Our mission is to bring accessible. affordable, engaging, and hoghly effective higher education to the world. We believe that higher education is basic human right, and we seek to empower our students to advance their education and careers.</p><br>
					<p>Education is no longer a one-time event but a lifelong experience. Education should be less passive listening (no long lectures) and more active doing Education should.</p>
				</div>
				<div class="col-md-5 col-sm-4">
					<div  id="about-3" class="embed-responsive embed-responsive-16by9">

					</div>
				</div>
			</div>
		</div>
		<!--<div class="container">
			<div class="underlined-title text-center">
				<h3>Our Staff</h3>
				<hr>
			</div>
			<div class="row">
				<div class="col-md-5 col-sm-4">
					<div  id="about-4" class="embed-responsive embed-responsive-16by9">

					</div>
				</div>
				<div class="col-md-7 col-sm-8">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>
				</div>
			</div>-->
</div>


	@endsection

	@section('content-js')
	<script type="text/javascript">
	function loadyt(n){
		var yt_id = [
			'bkwdBBv64tw',
			'QqVqBomzDOQ',
			'xEGTNbLckVk'
		]
		m = parseInt(n)+1;
		$('#about-'+m).html('<iframe style="display:none" type="text/html" class="ytplayer embed-responsive-item"'+
'src="https://www.youtube.com/embed/'+yt_id[n]+'?autoplay=0&showinfo=0&controls=0&modestbranding=1&loop=1&rel=0&playlist='+yt_id[n]+'&color=pink"&'+
'frameborder="0"></iframe>');
		$('#advantage-'+m).find('.ytplayer').ready(function() {

			//				$(this).show();
											$(this).find('.ytplayer').fadeIn( "slow", function() {
	// Animation complete
});
			if((m)<yt_id.length){
				setTimeout(function(){
					loadyt(m)
				},800)
			}
		})
	}

	$( window ).on( "load", function() {
			loadyt(0);

	})
	</script>
	@endsection


<a class="list-group-item {{ isset($active) && $active == 'section' ? 'active' : '' }}" href="{{ url('/admin/section') }}">
	List Section
</a>

<a class="list-group-item {{ isset($active) && $active == 'add' ? 'active' : '' }}" href="{{ url('/admin/section/add') }}">
	Add Section
</a>
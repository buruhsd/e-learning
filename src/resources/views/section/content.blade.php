@extends('master', ['active' => 'section'])
@section('sidebar')
    @include('section.sidebar', ['active' => 'add'])
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Content Manager</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ url('/admin/manager') }}"> Back</a>

            </div>

        </div>

    </div>


    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
            <table class="table-responsive table table-bordered">
                    <tr>
                        <th style="width:30%">Section name</th>
                        <td>{{$section->name}}</td>
                    </tr>
                
                    <tr>
                        <th style="width:30%">Chapter</th>
                        <td>{{$section->chapter-> name}}</td>
                    </tr>

                    <tr>
                        <th style="width:30%">Description</th>
                        <td>{{$section->description}}</td>
                    </tr>
                    
                    

            </table>

            <h4>Content List</h4>
            <div class="form-group table-responsive">
                <table class="table-responsive table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:5%">No</th>
                            <th style="width:30%">Content</th>
                            <th style="width:55%">Description</th>
                            <th style="width:10%">Action</th>
                        </tr>
                    </thead>
                    @if(count($content) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($content as $key => $contents)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $contents->name }}</td>
                                <td>{{ $contents->description }}</td>
                                <td>
                                    <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                    <!-- <a class="btn btn-xs btn-success" target="_blank" href="{{ action('ContentController@view', $contents->id) }}">view</a>
                                    -->
                                    <!--<a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#viewModal" data-file="{{ $contents->id }}">view</a>
                                    -->
                                    <a class="btn btn-xs btn-danger" href="{{action('ContentController@detil', $contents->id)}}" >view</a>

                                    <a class="btn btn-xs btn-danger" href="#" onclick="return confirm('Are you sure you want to delete this item?')">delete</a>
                                </td>
                            </tr>
                        @endforeach
                        

                </table>
            </div>
            
            <tr>
                <a class="btn btn-primary" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#myModal">Add Content</a>
            </tr>

@endsection

@section('content-modal')

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Content</h4>
      </div>
      <div class="modal-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ContentController@store', [$section->id]),
                'enctype'=> 'multipart/form-data', 'files',
                'method' => 'post'
            ])
        !!}

		@include('form.text', [
            'field' => 'name',
            'label' => 'Name',
            'placeholder' => 'Name',
            'default' => ''
        ])        

        {!! Form::hidden('section_id',$section->id, ['class'=>'form-control']) !!}
        {!! $errors->first('section_id', '<p class="help-block">:message</p>') !!}

        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Message',
            'attributes' => [
                'rows' => 3
            ],
            'default' => ''
        ])

        @include('form.file', [
            'field' => 'file',
            'label' => 'File',
            'placeholder' => 'file',
            
        ])

            {!! Form::hidden('date',\Carbon\Carbon::now(), ['class'=>'form-control', 'id' => 'date']) !!}
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}

       
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="viewModalLabel">View Content</h4>
      </div>
      <div class="modal-body">
        
        @if(count($content)==0)
            <td>There is no file preview</td>
        @else
            <div class="modal-body">
               <!-- <video width="320" height="240" autoplay>
                    <source src="{{asset('storage/'.$contents->file)}}" type="{{$contents->type}}">
                Your browser does not support the video tag.
                </video> 
                -->
                <video width="320" height="240" controls>
                        <source src="{{ route('getVideo', $contents->id)  }}" type="{{$contents->type}}">
                        Your browser does not support the video tag.
                </video>
                <!-- <img src="{{asset('storage/'.$contents->file)}}" id="show_file" width="500" height="500" alt=""> -->
            </div>
        @endif        

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

@endsection
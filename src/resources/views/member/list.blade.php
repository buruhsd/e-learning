@extends('master', ['active' => 'member'])
@section('sidebar')
<a class="list-group-item active" href="{{ url('/admin/member') }}">
    Member List
</a>
@endsection
@section('content')

<div class="container-fluid">
    <div class="col-xs-12">
        <div class="row" style="margin-left: -30px">
            {!!
                Form::open(array(
                    'class' => 'form-signin',
                    'role' => 'form',
                    'url' => action('UserController@memberlist'),
                    'method' => 'get',
                    ))
                    !!}

        <div class="col-md-2">
        @include('form.option', [
            'field' => 'suspended',
            'label' => 'Status',
            'options' => ['ALL' => 'All', true => 'Active', 'false' => 'Inactive'],
            'default' => (request()->input('suspended') ? request()->input('suspended') : 'ALL')
            ])
        </div>
        <div class="col-md-4">
        @include('form.text', [
            'label' => 'Find by',
            'field' => 'search',
            'placeholder' => "Search with name, email, phone",
            'default' => (request()->input('search') ? request()->input('search') : '')
            ])
        </div>
            <div class="col-md-2" style="margin-top : 25px">
                <button type="submit" class="btn btn-primary">Search</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-banner">

                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($users) == 0)
                                        <tr>
                                            <td colspan="5">There is no data.</td>
                                        </tr>
                                        @endif
                                        @foreach ($users as $key => $user)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if($user->suspended==true)
                                                <a class="btn-success btn-xs" disabled>Active</a>

                                                @else
                                                <a class="btn-warning btn-xs" disabled>Inactive</a>
                                                @endif
                                            </td>
                                            <td>
                                                <!-- <a class="btn btn-primary btn-xs" href="#">edit password</a> -->
                                                <!-- <a class="btn btn-primary btn-xs show-modal" href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-name="{{ $user->name }}" data-email="{{ $user->email }}" data-address="{{ $user->address }}" data-country="{{ $user->country }}" data-Company="{{ $user->company }}" data-target="#showModal">detail</a> -->

                                    <!-- <a class="btn btn-xs btn-danger" href="{{ action('UserController@delete', $user->id) }}" onclick="return confirm('Are you sure you want to delete `{{$user->name}}`?')">delete</a>
                                -->
                                @if($user->suspended==true)
                                <a class="btn btn-xs btn-warning" href="{{ action('UserController@suspend', $user->id) }}" onclick="return confirm('Are you sure you want to inactive `{{$user->name}}`?')">suspend</a>
                                @else
                                <a class="btn btn-xs btn-success" href="{{ action('UserController@unsuspend', $user->id) }}" onclick="return confirm('Are you sure you want to activate `{{$user->name}}`?')">activate</a>
                                @endif

                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="pull-right">
                {!! $users->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('content-modal')
<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail</h4>
    </div>
    <div class="modal-body">
        <div class="table-responsive">
            <table align="left" style="margin-top: 10px; margin-bottom: 20px; width: 100%; padding-right: 10px; padding-left: 10px">
                <td></td>
                <tr>
                    <th align="left">Name</th>
                    <td>:</td>
                    <td id="user_name"></td>
                </tr>
                <tr>
                    <th align="left">Email</th>
                    <td>:</td>
                    <td id="user_email"></td>
                </tr>

            </table>
        </div>  
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>
</div>
@endsection

@section('content-js')
<script>
    $('.show-modal').on('click', function(){
        var name = $(this).data('name');
        $('#user_name').html(name);
        var email = $(this).data('email');
        $('#user_email').html(email);
    });
</script>

@endsection
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">

   <!-- CSRF Token -->
   <meta name="csrf-token" content="{{ csrf_token() }}">

   <title>{{ config('app.name', 'Laravel') }}</title>

   <!-- Styles -->
   <link rel="stylesheet" href="{{ elixir("css/app.css") }}">
   <link rel="stylesheet" href="{{ elixir("css/font-awesome.css") }}">
   <link rel="stylesheet" href="{{ elixir("css/frontend.css") }}">
   <link rel="shortcut icon" href="{{{ asset('images/favicon.ico') }}}">
   	<link rel="stylesheet" href="{{ elixir("bower/jquery-bar-rating/dist/themes/fontawesome-stars.css") }}">
    	<link rel="stylesheet" href="{{ elixir("bower/jquery-bar-rating/dist/themes/fontawesome-stars-o.css") }}">
   @yield("content-css")
</head>
<body>
   <nav class="navbar navbar-default navbar-fixed-top navbar-default-custom">
      <div class="container">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/home') }}"><img class="img-logo" src="/images/logo_semestawebtv.png"></a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right navbar-nav-customs">
               {!! Html::smartNav(route('home'), 'Home') !!}
               {!! Html::smartNav(route('about'), 'About Us') !!}
               {!! Html::smartNav(route('subjects'), 'Subjects') !!}
               {!! Html::smartNav(route('contactus'), 'Contact Us') !!}
               @if (Auth::check())
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                 </a>
                 <ul class="dropdown-menu" role="menu">
                    @role(['admin', 'teacher'])
                    <li><a href="/admin/home"><i class='fa fa-dashboard'></i> Dashboard</a></li>
                    <li class="divider"></li>
                    @endrole
                    <li>
                       <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-btn fa-sign-out"></i> Logout</a>

                       <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                     </form>
                  </li>
               </ul>
            </li>
            @endif
            @if (Auth::guest())
            {!! Html::smartNav(route('login'), 'Sign In / Sign Up') !!}
            @endif
         </ul>
      </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
</nav>
@yield('header')
@yield('landing')

@yield('content')
<footer style="z-index:0">
   <div class="container">
      <div class="row footer-contact">
        <div class="col-sm-12">

             <center><h3 class="white-text  footer-title">Contact Us</h3></center>
        </div>
         <div class="col-sm-6">
            <iframe width="100%" height="315px" src="{{config('settings.video_url')}}" frameborder="0" allowfullscreen></iframe>

         </div>
         <div class="col-sm-6">

            <div class="col-md-12">
               <div class="social-contact">
                 <div class="col-sm-4 col-xs-6">
                   <div class="footer-icon">
                      <i class="green-text fa fa-envelope"></i>
                   </div>
                   <div class="footer-icon-text">
                      <p class="white-text">{{config('settings.email')}}</p>
                   </div>
                 </div>
                  {{-- facebook --}}
                  <div class="col-sm-4 col-xs-6">

                    <div class="footer-icon">
                       <i class="green-text fa fa-facebook-square"></i>
                    </div>
                    <a href="{{config('settings.facebook')}}" class="white-text">
                    <div class="footer-icon-text" style="margin-bottom: 10px;">
                       SemestaWebTv
                    </div>
                    </a>
                  </div>

                  {{-- twitter --}}
                  <div class="col-sm-4 col-xs-6">
                    <div class="footer-icon">
                       <i class="green-text fa fa-twitter-square"></i>
                    </div>
                    <div class="footer-icon-text" style="margin-bottom: 10px;">
                       <a href="{{config('settings.twitter')}}"  class="white-text">@SemestaWebTv</a>
                    </div>
                  </div>
                  {{-- youtube --}}
                  <div class="col-sm-4 col-xs-6">
                    <div class="footer-icon">
                       <i class="green-text fa fa-youtube-square"></i>
                    </div>
                    <div class="footer-icon-text" style="margin-bottom: 10px;">
                       <a href="{{config('settings.youtube')}}"  class="white-text">SemestaWebTv</a>
                    </div>
                  </div>
                  {{-- twitter --}}
                  <div class="col-sm-4 col-xs-6">
                    <div class="footer-icon">
                       <i class="green-text fa fa-instagram"></i>
                    </div>
                    <div class="footer-icon-text" style="margin-bottom: 10px;">
                       <a href="{{config('settings.instagram')}}"  class="white-text">SemestaWebTv</a>
                    </div>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
      <br class=" footer-contact">
      <hr class=" footer-contact">
      <div class="row">
         <div class="col-sm-6 col-xs-12">
            <div>
               <img src="{{ url('/') }}/images/logo_semestawebtv.png" class="logo-footer">
            </div>
            <br>
            <p class="white-text copyright-text">Copyright &copy; 2017 Semesta Web TV, All Right Reserved</p>
         </div>
         <div class="col-sm-6 col-xs-12">
            <div class="col-sm-12">
               <ul class="copy-list">
                  <li><a href="{{ url('/term-of-use') }}">Terms Of Use</a></li>
                  <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
               </ul>
            </div>
            <div class="col-sm-12">
               <br>
               <p class="white-text text-right">Developed by <a href="http://durenworks.com">Durenworks</a></p>
            </div>
         </div>
      </div>
   </div>
</footer>
<!-- Scripts -->
<script src="{{ asset(elixir("js/app.js")) }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
@yield('content-js')
</body>
</html>

<div class="modal fade" id="ladderModal">
  	<div class="modal-dialog">
		<div class="modal-content">
		  
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">ladder list</h4>
		  </div>
		  
		  <div class="modal-body">
		  	<div class="shade-screen">
		  		<div class="shade-screen hidden">
		  			<img src="/image/ajax-loader.gif">
		  		</div>
		  	</div>
		  	<div class="form-inline form-search">
		  		<input type="text" id="ladderSearch" class="form-control" style="width:88%" placeholder="ladder">
		  		<button class="btn btn-warning btn-sm" type="button" id="ladderButtonSrc">Search</button>
		  	</div>
			
			<div id="ladderTable"></div>			
		  
		  </div>
		  
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">close</button>				
		  </div>
		</div>
 	</div>
</div>
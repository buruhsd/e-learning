<div class="table-responsive">
<table class="table table-responsive">
	<thead>
	  <tr>
	  	<th>No</th>
		<th>Name</th>
		<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach($lists as  $list)
			<tr>
				<td>
					{{ $list->id }}
				</td>
				<td>
					{{ $list->name }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-warning btn-xs btn-choose" 
						type="button" data-id="{{ $list->id }}" data-name="{{ $list->name }}">Choose</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
</div>
{!! $lists->appends(Request::except('page'))->render() !!}

<a class="list-group-item {{ isset($active) && $active == 'ladder' ? 'active' : '' }}" href="{{ url('/admin/ladder') }}">
	Ladder List 
</a>

<a class="list-group-item {{ isset($active) && $active == 'add' ? 'active' : '' }}" href="{{ url('/admin/ladder/add') }}">
	Add Ladder
</a>
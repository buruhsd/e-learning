@extends('master', ['active' => 'message'])
@section('sidebar')
<a class="list-group-item active" href="{{ url('/admin/message') }}">
    Messages List
</a>
@endsection
@section('content')


<div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <table class="table table-banner">
                
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($message) == 0)
                        <tr>
                            <td colspan="5">There is no data.</td>
                        </tr>
                        @endif
                        @foreach ($message as $key => $inbox)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $inbox->name }}</td>
                                <td>{{$inbox->email}}</td>
                                <td>{{ $inbox->message }}</td>
                                <td>
                                    <a class="btn btn-xs btn-danger" href="#" onclick="return confirm('Are you sure you want to move this item to trash?')">delete</a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                
            </div>
            <div class="pull-right">
                {!! $message->render() !!}
            </div>
        </div>
    </div>

@endsection
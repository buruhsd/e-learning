<div class="modal fade" id="subjectModal">
  	<div class="modal-dialog">
		<div class="modal-content">
		  
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Subject list</h4>
		  </div>
		  
		  <div class="modal-body">
		  	<div class="shade-screen">
		  		<div class="shade-screen hidden">
		  			<img src="/image/ajax-loader.gif">
		  		</div>
		  	</div>
		  	<div class="form-inline form-search">
		  		<input type="text" id="subjectSearch" class="form-control" style="width:88%" placeholder="Subject">
		  		<button class="btn btn-warning btn-sm" type="button" id="subjectButtonSrc">Search</button>
		  	</div>
			
			<div id="subjectTable"></div>			
		  
		  </div>
		  
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">close</button>				
		  </div>
		</div>
 	</div>
</div>
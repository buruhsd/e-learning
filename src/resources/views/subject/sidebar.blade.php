
<a class="list-group-item {{ isset($active) && $active == 'subject' ? 'active' : '' }}" href="{{ url('/admin/subject') }}">
	Subject List 
</a>

<a class="list-group-item {{ isset($active) && $active == 'add' ? 'active' : '' }}" href="{{ url('/admin/subject/add') }}">
	Add Subject
</a>
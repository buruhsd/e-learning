
<a class="list-group-item {{ isset($active) && $active == 'teacher' ? 'active' : '' }}" href="{{ url('/admin/teacher') }}">
	Teacher List
</a>

<a class="list-group-item {{ isset($active) && $active == 'add' ? 'active' : '' }}" href="{{ url('/admin/user/add') }}">
	Add Teacher
</a>

<div class="separate-sidebar"></div>

{{-- <a class="list-group-item {{ isset($active) && $active == 'member' ? 'active' : '' }}" href="{{ url('/admin/user/member') }}">
	Member List
</a> --}}
@extends('master', ['active' => 'config'])
@section('sidebar')
@include('config.sidebar', ['active' => 'featured'])
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Featured Subject</h2>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
</div>
@if (count($errors) > 0)

<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-6">
        {!! Form::open(['role' => 'form','url' => action('ConfigController@featuredStore'),'method' => 'post' ])!!}

        @include('form.select', [
            'field' => 'featured_subject_1',
            'label' => 'Featured Subject 1',
            'options' => 
            [$featured_subject_1->id => $featured_subject_1->name]+App\Subject::pluck('name','id')->all(), null,              
            'default' =>  '',
            'hidden-value' => $featured_subject_1->id
            ])           

        @include('form.select', [
            'field' => 'featured_subject_2',
            'label' => 'Featured Subject 2',
            'options' => 
            [$featured_subject_2->id => $featured_subject_2->name]+App\Subject::pluck('name','id')->all(), null,              
            'default' =>  '',
            'hidden-value' => $featured_subject_2->id
            ])

        @include('form.select', [
            'field' => 'featured_subject_3',
            'label' => 'Featured Subject 3',
            'options' => 
            [$featured_subject_3->id => $featured_subject_3->name]+App\Subject::pluck('name','id')->all(), null,              
            'default' =>  '',
            'hidden-value' => $featured_subject_3->id
            ])

        @include('form.select', [
            'field' => 'featured_subject_4',
            'label' => 'Featured Subject 4',
            'options' => 
            [$featured_subject_4->id => $featured_subject_4->name]+App\Subject::pluck('name','id')->all(), null,              
            'default' =>  '',
            'hidden-value' => $featured_subject_4->id
            ])
            <hr>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div> 

    @endsection
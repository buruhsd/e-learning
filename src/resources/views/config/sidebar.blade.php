
<a class="list-group-item {{ isset($active) && $active == 'featured' ? 'active' : '' }}" href="{{ url('/admin/setting/featured') }}">
	Featured Subject
</a>

<a class="list-group-item {{ isset($active) && $active == 'slider' ? 'active' : '' }}" href="{{ url('/admin/setting/slider') }}">
	Slider
</a>

<a class="list-group-item {{ isset($active) && $active == 'website' ? 'active' : '' }}" href="{{ url('/admin/setting/website') }}">
	Website
</a>
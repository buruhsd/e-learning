@extends('master', ['active' => 'config'])
@section('sidebar')
@include('config.sidebar', ['active' => 'slider'])
@endsection

@section('content')

<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Edit Slider</h2>

        </div>

    </div>

</div>


@if (count($errors) > 0)

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

        {!!
            Form::open([
                'role' => 'form',
                'url' => action('ConfigController@updateSlider', $slider->id),
                'enctype'=> 'multipart/form-data',
                'method' => 'post'
            ])
        !!}

        @include('form.text', [
            'field' => 'name',
            'label' => 'Slider Name',
            'placeholder' => 'Slider Name',
            'default' => $slider->name
        ])

        @include('form.textarea', [
            'field' => 'description',
            'label' => 'Description',
            'placeholder' => 'Description',
            'attributes' => [
                'rows' => 3
            ],
            'default' => $slider->description
        ])

        @include('form.option', [
                'field' => 'status',
                'label' => 'Status',
                'options' => [
                    'published' => 'Published',
                    'unpublished'  => 'Unpublished',
                    ],              
                
            ])

        @include('form.file', [
            'field' => 'image',
            'label' => 'image',
            'placeholder' => 'image',
            
        ])
       
       <img class="thumbnail" src="{{ action('ConfigController@images', $slider->image) }}" height="170" style="display:inline; margin-right:20px;">
        
       <div class="form-group">
          <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}

@endsection
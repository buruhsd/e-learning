<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@frontend')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/subjects', 'SubjectController@all')->name('subjects');
Route::get('/subject/{id}', 'SubjectController@show')->name('detail_subject');
Route::get('/section/{id}', 'SectionController@show')->middleware('auth');
Route::post('/section/{id}', 'FeedbackController@store');
Route::get('dropbox-upload-file', 'FileController@dropboxFileUpload');
Route::get('dropbox', 'ContentController@dropboxFileUpload');

Auth::routes();
Route::get('auth/verify/{token}', 'Auth\RegisterController@verify');
Route::get('auth/send-verification', 'Auth\RegisterController@sendVerification');

Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

Route::get('/googleauth', 'SocialAuthController@googleauth');
Route::get('/google-callback', 'SocialAuthController@googlecallback');
Route::get('images/{images}', 'SubjectController@images');

Route::get('slider/images/{images}', 'ConfigController@images')->name('slider_image');


Route::get('/course/{id}', 'CourseController@show')->middleware('auth');
Route::post('/course/join/{id}', 'CourseController@join')->middleware('auth');
Route::post('/course/leave/{id}', 'CourseController@leave')->middleware('auth');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin' or 'teacher']], function () {
	Route::get('/home', 'HomeController@index');
	Route::get('/teacher', 'UserController@index');
	Route::get('/user/add', 'UserController@add');
	Route::post('/user/add', 'UserController@store');
	Route::get('/user/edit/{id}', 'UserController@edit');
	Route::post('/user/edit/{id}', 'UserController@update');
	Route::get('/member', 'UserController@memberlist');
	Route::get('/user/{id}/delete', 'UserController@delete');
	Route::get('/user/{id}/suspend', 'UserController@suspend');
	Route::get('/user/{id}/unsuspend', 'UserController@unsuspend');

	Route::get('/ladder', 'LadderController@index');
	Route::get('/ladder/add', 'LadderController@add');
	Route::post('/ladder/add', 'LadderController@store');
	Route::get('/ladder/edit/{id}', 'LadderController@edit');
	Route::post('/ladder/edit/{id}', 'LadderController@update');
	Route::get('/ladder/{id}/delete', 'LadderController@delete');
	Route::get('/ladder/{id}/active', 'LadderController@active');
	Route::get('/ladder/{id}/inactive', 'LadderController@inactive');

	Route::get('/subject', 'SubjectController@index');
	Route::get('/subject/add', 'SubjectController@add');
	Route::post('/subject/add', 'SubjectController@store');
	Route::get('/subject/edit/{id}', 'SubjectController@edit');
	Route::post('/subject/edit/{id}', 'SubjectController@update');
	Route::get('/subject/{id}/delete', 'SubjectController@delete');
	Route::get('/subject/{id}/active', 'SubjectController@active');
	Route::get('/subject/{id}/inactive', 'SubjectController@inactive');

	Route::get('/password', 'SettingController@editpassword');
	Route::post('/password', 'SettingController@updatePassword');

	Route::get('/course', 'CourseController@index');
	Route::get('/course/add', 'CourseController@add');
	Route::post('/course/add', 'CourseController@store');
	Route::get('/course/edit/{id}', 'CourseController@edit');
	Route::post('/course/edit/{id}', 'CourseController@update');
	Route::get('/course/{id}/delete', 'CourseController@delete');
	Route::get('/course/{id}/soft', 'CourseController@softdelete');
	Route::get('/course/trash', 'CourseController@trash');
	Route::get('/course/trash/{id}/restore', 'CourseController@restore');
	Route::get('/course/{id}/mystudent', 'CourseController@mystudent');

    // Route::get('/chapter', 'ChapterController@index');
	Route::get('/chapter', function(){
		$chapters = App\Chapter::all();
		return view('chapter.index', compact('chapters'));
	});

	Route::get('/chapter/{id}', 'ChapterController@chaptermanager');

	Route::get('/ajax/{id_chapter}', 'ChapterController@ajax');

	Route::get('/content-ajax/{id_content}', 'ChapterController@content');

	Route::get('/chapter/add', 'ChapterController@add');
	Route::post('/chapter/add', 'ChapterController@store');
	Route::get('/chapter/edit/{id}', 'ChapterController@edit');
	Route::post('/chapter/edit/{id}', 'ChapterController@update');
	Route::get('/chapter/{id}/delete', 'ChapterController@delete');

	Route::get('content/add', 'ContentController@add');
	Route::post('content/add', 'ContentController@addcontent');
	Route::get('content/detil/{id}', 'ContentController@detil');
	Route::get('content/quiz/{id}', 'ContentController@detilquiz');

	Route::post('content/videos/', 'ContentController@htmlstore');
	Route::post('content/quiz/', 'ContentController@quizstore');
	Route::post('/section/addcontent', 'ContentController@pdfstore');
	Route::post('/section/addcontentvideo', 'ContentController@videostore');

	Route::post('/section/{id}/update', 'ContentController@updateSectionContent');

	Route::get('/delete/{id}/pdf', 'ContentController@deletepdf');
	Route::get('/delete/{id}/video', 'ContentController@deletevideo');
	Route::get('/delete/{id}/html', 'ContentController@deletehtml');
	Route::get('/delete/{id}/quiz', 'ContentController@deletequiz');
	Route::get('/delete/{id}/content', 'ContentController@deleteSectionContent');

	Route::get('/section', 'SectionController@index');
	Route::get('/section/add', 'SectionController@add');
	Route::post('/section/add', 'SectionController@store');
	Route::get('/section/edit/{id}', 'SectionController@edit');
	Route::post('/section/edit/{id}', 'SectionController@update');
	Route::get('/section/{id}/delete', 'SectionController@delete');

	Route::get('/section/{id}/addcontent', 'ContentController@add');


	Route::get('/feedback', 'FeedbackController@index');
	Route::get('/feedback/{id}/view', 'FeedbackController@view');
	Route::get('/feedback/{id}/approve', 'FeedbackController@approve');
	Route::get('/feedback/{id}/unapprove', 'FeedbackController@unapprove');
	Route::get('/feedback/{id}/delete', 'FeedbackController@delete');
	Route::post('/feedback/{id}/reply', 'FeedbackController@reply');
	Route::post('/feedback/{id}/update', 'FeedbackController@updateReply');
	Route::get('/feedback/{id}/delete', 'FeedbackController@deleteReply');
	//Route::get('/download/get/{file}', 'ContentController@view');

	Route::get('/setting/featured', 'ConfigController@featured');
	Route::post('/setting/featured', 'ConfigController@featuredStore');

	Route::get('/setting/website', 'ConfigController@website');
	Route::post('/setting/website', 'ConfigController@websiteStore');

	Route::get('/setting/slider', 'ConfigController@slider');
	Route::post('/setting/slider', 'ConfigController@sliderStore');
	Route::get('/setting/slider/{id}/edit','ConfigController@editSlider');
	Route::post('/setting/slider/{id}/edit','ConfigController@updateSlider');
	Route::get('/setting/slider/{id}/delete','ConfigController@deleteSlider');

	Route::get('/content/list', 'ContentController@picklist');
	Route::get('/repo', 'RepoController@index');
	Route::get('/repo/add', 'ContentController@add');
	Route::post('repo/add/existing', 'ContentController@existing');
	Route::post('/repo/addcontent', 'RepoController@pdfstore');
	Route::post('/repo/addcontentvideo', 'RepoController@videostore');
	Route::post('repo/videos/', 'RepoController@htmlstore');
	Route::post('repo/quiz/', 'RepoController@quizstore');
	Route::post('repo/update/{id}', 'RepoController@update');

	Route::get('/message', 'MessageController@index');

	// Route::get('/logout', 'Auth\LoginController@logout');
	// Auth::routes();
});

Route::get('/subject/list/', 'SubjectController@picklist');
Route::get('/ladder/list/', 'LadderController@picklist');
Route::get('/download/get/{id}', 'ContentController@view');
Route::get('get-video/{id}', 'ContentController@getVideo')->name('getVideo');

Route::get('/view/{file}', function ($filename)
{
	return SectionContent::make(storage_path() . '/' . $file)->response();
});

Route::get('/nojs', 'HomeController@nojs');



// Auth::routes();

//Route::get('/home', 'HomeController@index');
Route::get('/home', function () {
	if (\Laratrust::hasRole(['admin', 'teacher']))
	{
		return redirect()->intended('/admin/home');
	}
	return redirect()->intended('/');
});

Route::get('/rate','RateControllers@index');
Route::put('/rate','RateControllers@update')->name('setrate');
Route::get('/contact-us','ContactUsController@index')->name('contactus');
Route::post('/contact-us', 'MessageController@store')->name('postmessage');



Route::get('/term-of-use',function(){
	return view('frontend.term_of_use');
});
Route::get('/privacy-policy',function(){
	return view('frontend.privacy_policy');
});

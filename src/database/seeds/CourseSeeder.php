<?php

use Illuminate\Database\Seeder;
use App\Section;
use App\SectionContent;
use App\Course;
use App\Chapter;
use App\Feedback;
use App\Content;
use App\Rate;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //create sample Course
      $course1 = Course::create([
        'name'=>'Math For Dummies',
        'subject_id'=>1,
        'ladder_id'=>1,
        'teacher_id' => 2,
        'description'=>'sample description']);
      $course2 = Course::create(['name'=>'Math for Junior High School',
        'subject_id'=>1,
        'ladder_id'=>2,
        'teacher_id' => 2,
        'description'=>'sample description']);
      $course3 = Course::create(['name'=>'Math for Senior High School',
        'subject_id'=>1,
        'ladder_id'=>3,
        'teacher_id' => 2,
        'description'=>'sample description']);

      //get sample student
      $student1 = \App\User::find(10)->id;
      $student2 = \App\User::find(11)->id;
      $student3 = \App\User::find(12)->id;

      //creaete course students
      //$course1->students()->attach([$student1, $student2]);
      $course2->students()->attach([$student1]);
      $course1->students()->attach([$student1, $student2, $student3]);

      //create sample chapter
      $chapter1 = Chapter::create(['name'=>'Number', 'course_id'=>$course1->id, 'description'=>'sample description']);
      $chapter2 = Chapter::create(['name'=>'Geometry', 'course_id'=>$course1->id, 'description'=>'sample description']);

      //create sample section
      $section1 = Section::create(['name'=>'Integers', 'chapter_id'=>$chapter1->id, 'description'=>'sample description']);
      $section2 = Section::create(['name'=>'Primes', 'chapter_id'=>$chapter1->id, 'description'=>'sample description']);

      //create sample content
      $content1 = Content::create(['name' => 'Number Material', 'type' => 'application/pdf', 'file' => 'phpgHnGHe.pdf', 'url' => '', 'category' => 'PDF', 'description' => 'Material for number basic number learning']);
      $content2 = Content::create(['name' => 'Quiz Video', 'type' => 'application/zip', 'file' => 'Untitled.html', 'url' => 'phpJPApCg/', 'category' => 'Html/Video', 'description' => 'Video learning for basic integer']);
      $content3 = Content::create(['name' => 'Quiz Time', 'type' => 'application/zip', 'file' => 'examviev.htm', 'url' => 'phpjDCGeD/', 'category' => 'Quiz', 'description' => 'Take some quiz to test your understanding']);

      //create sample section content
      $section_content1 = SectionContent::create(['section_id' => 1, 'content_id' => $content1->id, 'name' => 'sample name', 'description' => 'sample description']);
      $section_content2 = SectionContent::create(['section_id' => 1, 'content_id' => $content2->id, 'name' => 'sample name', 'description' => 'sample description']);
      $section_content3 = SectionContent::create(['section_id' => 1, 'content_id' => $content3->id, 'name' => 'sample name', 'description' => 'sample description']);

      //create sample feedbacks
      $faker = Faker\Factory::Create();

      $feedback1 = Feedback::create(['section_id'=>$section1->id,'user_id' => 9,'parent_id'=>0,'comment'=>$faker->realText($maxNbChars = 200, $indexSize = 2), 'status' => 'published']);
      $feedback2 = Feedback::create(['section_id'=>$section1->id,'user_id' => 10,'parent_id'=>0,'comment'=>$faker->realText($maxNbChars = 200, $indexSize = 2), 'status' => 'published']);
      $feedback3 = Feedback::create(['section_id'=>$section1->id,'user_id' => 1,'parent_id'=>1,'comment'=>$faker->realText($maxNbChars = 200, $indexSize = 2), 'status' => 'published']);
      $feedback4 = Feedback::create(['section_id'=>$section1->id,'user_id' => 10,'parent_id'=>0,'comment'=>$faker->realText($maxNbChars = 200, $indexSize = 2), 'status' => 'pending']);

      $rate1 = Rate::create(['section_id' => $section1->id, 'user_id' => $student1, 'value' => 4]);
      $rate2 = Rate::create(['section_id' => $section1->id, 'user_id' => $student2, 'value' => 5]);
      $rate3 = Rate::create(['section_id' => $section1->id, 'user_id' => $student3, 'value' => 3]);
    }
}
